<div class="edgtf-album-track-list <?php echo esc_attr($alb_skin); ?>" id="<?php echo esc_attr($random_id); ?>">
<?php
	$i = 1;
	foreach($tracks as $track):?>
		<div class="edgtf-album-track" data-track-order="<?php echo esc_attr($i); ?>">
			<span class="edgtf-at-title">
				<?php if(isset($track['title']) && $track['title'] != '') : ?>
					<span class="edgtf-at-number"><?php echo esc_attr($i).'. '; ?></span><?php echo esc_attr($track['title']); ?>
				<?php endif; ?>
			</span>
			<span class="edgtf-at-time"><?php echo esc_attr($track['track_time']); ?></span>
			<a class="edgtf-at-play-button">
				<audio src="<?php echo esc_url($track['track_file']); ?>"></audio>
				<i class="fa fa-play edgtf-at-play-icon"></i><i class="fa fa-pause edgtf-at-pause-icon"></i>
			</a>
			<span class="edgtf-at-video-button-holder">
				<?php if(isset($track['video_link']) && $track['video_link'] != '') : ?>
					<a class="edgtf-at-video-button" href="<?php echo esc_url($track['video_link']); ?>" data-rel="prettyPhoto" >
						<i class="fa fa-video-camera"></i>
					</a>
				<?php endif; ?>
			</span>
			<span class="edgtf-at-download-holder">
				<?php if(isset($track['free_download']) && $track['free_download'] == 'yes') : ?>
					<a href="<?php echo esc_url($track['track_file']); ?>" class="edgtf-at-download" download><i class="fa fa-download"></i></a>
				<?php endif; ?>
			</span>
		</div>
<?php
		$i++;
	endforeach;
?>
</div>