<?php

if ( ! function_exists( 'boldlab_core_add_product_list_shortcode' ) ) {
	/**
	 * Function that is adding shortcode into shortcodes list for registration
	 *
	 * @param array $shortcodes - Array of registered shortcodes
	 *
	 * @return array
	 */
	function boldlab_core_add_product_list_shortcode( $shortcodes ) {
		$shortcodes[] = 'BoldlabCoreProductListShortcode';

		return $shortcodes;
	}

	add_filter( 'boldlab_core_filter_register_shortcodes', 'boldlab_core_add_product_list_shortcode' );
}

if ( class_exists( 'BoldlabCoreListShortcode' ) ) {
	class BoldlabCoreProductListShortcode extends BoldlabCoreListShortcode {

		public function __construct() {
			$this->set_post_type( 'product' );
			$this->set_post_type_taxonomy( 'product_cat' );
			$this->set_post_type_additional_taxonomies( array( 'product_tag', 'product_type' ) );
			$this->set_layouts( apply_filters( 'boldlab_core_filter_product_list_layouts', array() ) );
			$this->set_extra_options( apply_filters( 'boldlab_core_filter_product_list_extra_options', array() ) );

			parent::__construct();
		}

		public function map_shortcode() {
			$this->set_shortcode_path( BOLDLAB_CORE_INC_URL_PATH . '/woocommerce/shortcodes/product-list' );
			$this->set_base( 'boldlab_core_product_list' );
			$this->set_name( esc_html__( 'Product List', 'boldlab-core' ) );
			$this->set_description( esc_html__( 'Shortcode that displays list of products', 'boldlab-core' ) );
			$this->set_category( esc_html__( 'Boldlab Core', 'boldlab-core' ) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'custom_class',
				'title'      => esc_html__( 'Custom Class', 'boldlab-core' )
			) );
			$this->map_list_options();
			$this->map_query_options( array( 'post_type' => $this->get_post_type() ) );
			$this->map_layout_options( array( 'layouts' => $this->get_layouts() ) );
			$this->set_option( array(
				'field_type' => 'select',
				'name'       => 'display_category',
				'title'      => esc_html__( 'Display Category', 'boldlab-core' ),
				'options'    => boldlab_core_get_select_type_options_pool( 'yes_no' ),
				'group'      => esc_html__( 'Layout', 'boldlab-core' )

			) );

			if ( get_option( 'woocommerce_enable_reviews' ) !== 'no' && get_option( 'woocommerce_enable_review_rating' ) !== 'no' ) {
				$this->set_option( array(
					'field_type' => 'select',
					'name'       => 'display_rating',
					'title'      => esc_html__( 'Display Rating', 'boldlab-core' ),
					'options'    => boldlab_core_get_select_type_options_pool( 'yes_no' ),
					'group'      => esc_html__( 'Layout', 'boldlab-core' )

				) );
			}
			$this->map_additional_options();
			$this->map_extra_options();
		}

		public static function call_shortcode( $params ) {
			$html = qode_framework_call_shortcode( 'boldlab_core_product_list', $params );
			$html = str_replace( "\n", '', $html );

			return $html;
		}

		public function render( $options, $content = null ) {
			parent::render( $options );

			$atts = $this->get_atts();

			$atts['post_type']       = $this->get_post_type();
			$atts['taxonomy_filter'] = $this->get_post_type_taxonomy();

			// Additional query args
			$atts['additional_query_args'] = $this->get_additional_query_args( $atts );

			$atts['holder_classes'] = $this->get_holder_classes( $atts );
			$atts['query_result']   = new \WP_Query( boldlab_core_get_query_params( $atts ) );
			$atts['justified_attr'] = $this->get_justified_data( $atts );
			$atts['slider_attr']    = $this->get_slider_data( $atts );
			$atts['title_styles']   = $this->get_title_styles( $atts );
			$atts['data_attr']      = boldlab_core_get_pagination_data( BOLDLAB_CORE_REL_PATH, 'woocommerce/shortcodes', 'product-list', 'product', $atts );

			$atts['this_shortcode'] = $this;

			return boldlab_core_get_template_part( 'woocommerce/shortcodes/product-list', 'templates/content', $atts['behavior'], $atts );
		}

		private function get_holder_classes( $atts ) {
			$holder_classes = $this->init_holder_classes();

			$holder_classes[] = 'qodef-woo-shortcode';
			$holder_classes[] = 'qodef-woo-product-list';
			$holder_classes[] = ! empty( $atts['layout'] ) ? 'qodef-item-layout--' . $atts['layout'] : '';

			$list_classes   = $this->get_list_classes( $atts );
			$holder_classes = array_merge( $holder_classes, $list_classes );

			return implode( ' ', $holder_classes );
		}

		public function get_item_classes( $atts ) {
			$item_classes      = $this->init_item_classes();
			$list_item_classes = $this->get_list_item_classes( $atts );

			$item_classes = array_merge( $item_classes, $list_item_classes );

			return implode( ' ', $item_classes );
		}

		private function get_title_styles( $atts ) {
			$styles = array();

			if ( ! empty( $atts['text_transform'] ) ) {
				$styles[] = 'text-transform: ' . $atts['text_transform'];
			}

			return $styles;
		}
	}
}