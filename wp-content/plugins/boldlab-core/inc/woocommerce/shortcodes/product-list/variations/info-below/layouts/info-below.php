<div <?php wc_product_class( $item_classes ); ?>>
	<div class="qodef-woo-product-inner">
		<?php if ( has_post_thumbnail() ) { ?>
			<div class="qodef-woo-product-image">
				<?php boldlab_core_template_part( 'woocommerce/shortcodes/product-list', 'templates/post-info/on-sale' ); ?>
				<?php boldlab_core_template_part( 'woocommerce/shortcodes/product-list', 'templates/post-info/image', '', $params ); ?>
			</div>
		<?php } ?>
		<div class="qodef-woo-product-content">
			<?php boldlab_core_template_part( 'woocommerce/shortcodes/product-list', 'templates/post-info/category', '', $params ); ?>
			<?php boldlab_core_template_part( 'woocommerce/shortcodes/product-list', 'templates/post-info/title', '', $params ); ?>
			<?php boldlab_core_template_part( 'woocommerce/shortcodes/product-list', 'templates/post-info/rating', '', $params ); ?>
			<div class="qodef-woo-product-price-holder">
				<?php boldlab_core_template_part( 'woocommerce/shortcodes/product-list', 'templates/post-info/add-to-cart' ); ?>
				<?php boldlab_core_template_part( 'woocommerce/shortcodes/product-list', 'templates/post-info/price', '', $params ); ?>
			</div>
		</div>
		<?php boldlab_core_template_part( 'woocommerce/shortcodes/product-list', 'templates/post-info/link' ); ?>
	</div>
</div>