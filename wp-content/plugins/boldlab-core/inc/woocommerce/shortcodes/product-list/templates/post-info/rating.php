<?php

$product = boldlab_core_woo_get_global_product();
$display_rating = isset( $display_rating ) && $display_rating !== 'no' ? true: false;

if ( ! empty( $product ) && get_option( 'woocommerce_enable_reviews' ) !== 'no' && get_option( 'woocommerce_enable_review_rating' ) !== 'no' && $display_rating ) {
	$rating = $product->get_average_rating();
	
	if ( ! empty( $rating ) ) {
		echo boldlab_core_woo_product_get_rating_html( '', $rating, 0 );
	}
}