<?php if ( class_exists( 'BoldlabCoreSocialShareShortcode' ) ) { ?>
	<div class="qodef-woo-product-social-share">
		<?php
		$params = array();
		$params['title'] = esc_html__( 'Share:', 'boldlab-core' );
		
		echo BoldlabCoreSocialShareShortcode::call_shortcode( $params ); ?>
	</div>
<?php } ?>