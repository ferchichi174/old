<?php

if ( ! function_exists( 'boldlab_core_set_general_typography_styles' ) ) {
	/**
	 * Function that generates p typography styles
	 *
	 * @param $style string
	 *
	 * @return string
	 */
	function boldlab_core_set_general_typography_styles( $style ) {
		$scope = BOLDLAB_CORE_OPTIONS_NAME;
		
		$p_styles          = boldlab_core_get_typography_styles( $scope, 'qodef_p' );
		$h1_styles         = boldlab_core_get_typography_styles( $scope, 'qodef_h1' );
		$h2_styles         = boldlab_core_get_typography_styles( $scope, 'qodef_h2' );
		$h3_styles         = boldlab_core_get_typography_styles( $scope, 'qodef_h3' );
		$h4_styles         = boldlab_core_get_typography_styles( $scope, 'qodef_h4' );
		$h5_styles         = boldlab_core_get_typography_styles( $scope, 'qodef_h5' );
		$h6_styles         = boldlab_core_get_typography_styles( $scope, 'qodef_h6' );
		$link_styles       = boldlab_core_get_typography_styles( $scope, 'qodef_link' );
		$link_hover_styles = boldlab_core_get_typography_hover_styles( $scope, 'qodef_link' );

		if ( ! empty( $p_styles ) ) {
			$style .= qode_framework_dynamic_style( 'p', $p_styles );
		}

		if ( ! empty( $h1_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h1', $h1_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h1', $h1_styles );
		}

		if ( ! empty( $h2_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h2', $h2_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h2', $h2_styles );
		}

		if ( ! empty( $h3_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h3', $h3_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h3', $h3_styles );
		}

		if ( ! empty( $h4_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h4', $h4_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h4', $h4_styles );
		}
		
		if ( ! empty( $h5_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h5', $h5_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h5', $h5_styles );
		}
		
		if ( ! empty( $h6_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h6', $h6_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h6', $h6_styles );
		}
		
		if ( ! empty( $link_styles ) ) {
			$style .= qode_framework_dynamic_style( array('a', 'p a'), $link_styles );
		}
		
		if ( ! empty( $link_hover_styles ) ) {
			$style .= qode_framework_dynamic_style( array('a:hover', 'p a:hover'), $link_hover_styles );
		}

		return $style;
	}

	add_filter( 'boldlab_filter_add_inline_style', 'boldlab_core_set_general_typography_styles' );
}

if ( ! function_exists( 'boldlab_core_set_general_typography_responsive_1024_styles' ) ) {
	/**
	 * Function that generates p typography styles
	 *
	 * @param $style string
	 *
	 * @return string
	 */
	function boldlab_core_set_general_typography_responsive_1024_styles( $style ) {
		$scope = BOLDLAB_CORE_OPTIONS_NAME;
		
		$p_styles  = boldlab_core_get_typography_styles( $scope, 'qodef_p_responsive_1024' );
		$h1_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h1_responsive_1024' );
		$h2_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h2_responsive_1024' );
		$h3_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h3_responsive_1024' );
		$h4_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h4_responsive_1024' );
		$h5_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h5_responsive_1024' );
		$h6_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h6_responsive_1024' );
		
		if ( ! empty( $p_styles ) ) {
			$style .= qode_framework_dynamic_style( 'p', $p_styles );
		}
		
		if ( ! empty( $h1_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h1', $h1_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h1', $h1_styles );
		}
		
		if ( ! empty( $h2_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h2', $h2_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h2', $h2_styles );
		}
		
		if ( ! empty( $h3_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h3', $h3_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h3', $h3_styles );
		}
		
		if ( ! empty( $h4_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h4', $h4_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h4', $h4_styles );
		}
		
		if ( ! empty( $h5_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h5', $h5_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h5', $h5_styles );
		}
		
		if ( ! empty( $h6_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h6', $h6_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h6', $h6_styles );
		}
		
		return $style;
	}
	
	add_filter( 'boldlab_core_filter_add_responsive_1024_inline_style', 'boldlab_core_set_general_typography_responsive_1024_styles' );
}

if ( ! function_exists( 'boldlab_core_set_general_typography_responsive_768_styles' ) ) {
	/**
	 * Function that generates p typography styles
	 *
	 * @param $style string
	 *
	 * @return string
	 */
	function boldlab_core_set_general_typography_responsive_768_styles( $style ) {
		$scope = BOLDLAB_CORE_OPTIONS_NAME;
		
		$p_styles  = boldlab_core_get_typography_styles( $scope, 'qodef_p_responsive_768' );
		$h1_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h1_responsive_768' );
		$h2_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h2_responsive_768' );
		$h3_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h3_responsive_768' );
		$h4_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h4_responsive_768' );
		$h5_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h5_responsive_768' );
		$h6_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h6_responsive_768' );
		
		if ( ! empty( $p_styles ) ) {
			$style .= qode_framework_dynamic_style( 'p', $p_styles );
		}
		
		if ( ! empty( $h1_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h1', $h1_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h1', $h1_styles );
		}
		
		if ( ! empty( $h2_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h2', $h2_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h2', $h2_styles );
		}
		
		if ( ! empty( $h3_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h3', $h3_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h3', $h3_styles );
		}
		
		if ( ! empty( $h4_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h4', $h4_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h4', $h4_styles );
		}
		
		if ( ! empty( $h5_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h5', $h5_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h5', $h5_styles );
		}
		
		if ( ! empty( $h6_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h6', $h6_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h6', $h6_styles );
		}
		
		return $style;
	}
	
	add_filter( 'boldlab_core_filter_add_responsive_768_1024_inline_style', 'boldlab_core_set_general_typography_responsive_768_styles' );
}

if ( ! function_exists( 'boldlab_core_set_general_typography_responsive_680_styles' ) ) {
	/**
	 * Function that generates p typography styles
	 *
	 * @param $style string
	 *
	 * @return string
	 */
	function boldlab_core_set_general_typography_responsive_680_styles( $style ) {
		$scope = BOLDLAB_CORE_OPTIONS_NAME;
		
		$p_styles  = boldlab_core_get_typography_styles( $scope, 'qodef_p_responsive_680' );
		$h1_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h1_responsive_680' );
		$h2_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h2_responsive_680' );
		$h3_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h3_responsive_680' );
		$h4_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h4_responsive_680' );
		$h5_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h5_responsive_680' );
		$h6_styles = boldlab_core_get_typography_styles( $scope, 'qodef_h6_responsive_680' );
		
		if ( ! empty( $p_styles ) ) {
			$style .= qode_framework_dynamic_style( 'p', $p_styles );
		}
		
		if ( ! empty( $h1_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h1', $h1_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h1', $h1_styles );
		}
		
		if ( ! empty( $h2_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h2', $h2_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h2', $h2_styles );
		}
		
		if ( ! empty( $h3_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h3', $h3_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h3', $h3_styles );
		}
		
		if ( ! empty( $h4_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h4', $h4_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h4', $h4_styles );
		}
		
		if ( ! empty( $h5_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h5', $h5_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h5', $h5_styles );
		}
		
		if ( ! empty( $h6_styles ) ) {
			$style .= qode_framework_dynamic_style( 'h6', $h6_styles );
			$style .= qode_framework_dynamic_style( '.qodef-h6', $h6_styles );
		}
		
		return $style;
	}
	
	add_filter( 'boldlab_core_filter_add_responsive_680_inline_style', 'boldlab_core_set_general_typography_responsive_680_styles' );
}

if ( ! function_exists( 'boldlab_core_get_typography_styles' ) ) {
	/**
	 * Generates typography styles
	 *
	 * @param $scope string
	 * @param $field_name string
	 *
	 * @return array
	 */
	function boldlab_core_get_typography_styles( $scope, $field_name ) {
		$color           = qode_framework_get_option_value( $scope, 'admin', $field_name . '_color' );
		$font_family     = qode_framework_get_option_value( $scope, 'admin', $field_name . '_font_family' );
		$font_size       = qode_framework_get_option_value( $scope, 'admin', $field_name . '_font_size' );
		$line_height     = qode_framework_get_option_value( $scope, 'admin', $field_name . '_line_height' );
		$letter_spacing  = qode_framework_get_option_value( $scope, 'admin', $field_name . '_letter_spacing' );
		$font_weight     = qode_framework_get_option_value( $scope, 'admin', $field_name . '_font_weight' );
		$text_transform  = qode_framework_get_option_value( $scope, 'admin', $field_name . '_text_transform' );
		$font_style      = qode_framework_get_option_value( $scope, 'admin', $field_name . '_font_style' );
		$text_decoration = qode_framework_get_option_value( $scope, 'admin', $field_name . '_text_decoration' );
		$margin_top      = qode_framework_get_option_value( $scope, 'admin', $field_name . '_margin_top' );
		$margin_bottom   = qode_framework_get_option_value( $scope, 'admin', $field_name . '_margin_bottom' );
		
		$styles = array();
		
		if ( ! empty( $color ) ) {
			$styles['color'] = $color;
		}
		
		if ( isset( $font_family ) && $font_family !== false && $font_family !== '-1' && $font_family !== '' ) {
			$styles['font-family'] = qode_framework_get_formatted_font_family( $font_family );
		}
		
		if ( ! empty( $font_size ) ) {
			if ( qode_framework_string_ends_with( $font_size, 'px' ) || qode_framework_string_ends_with( $font_size, 'em' ) || qode_framework_string_ends_with( $font_size, 'rem' ) ) {
				$styles['font-size'] = $font_size;
			} else {
				$styles['font-size'] = intval( $font_size ) . 'px';
			}
		}
		
		if ( ! empty( $line_height ) ) {
			if ( qode_framework_string_ends_with( $line_height, 'px' ) || qode_framework_string_ends_with( $line_height, 'em' ) || qode_framework_string_ends_with( $line_height, 'rem' ) ) {
				$styles['line-height'] = $line_height;
			} else {
				$styles['line-height'] = intval( $line_height ) . 'px';
			}
		}
		
		if ( ! empty( $font_style ) ) {
			$styles['font-style'] = $font_style;
		}
		
		if ( ! empty( $font_weight ) ) {
			$styles['font-weight'] = $font_weight;
		}
		
		if ( ! empty( $text_decoration ) ) {
			$styles['text-decoration'] = $text_decoration;
		}
		
		if ( $letter_spacing !== '' ) {
			if ( qode_framework_string_ends_with( $letter_spacing, 'px' ) || qode_framework_string_ends_with( $letter_spacing, 'em' ) || qode_framework_string_ends_with( $letter_spacing, 'rem' ) ) {
				$styles['letter-spacing'] = $letter_spacing;
			} else {
				$styles['letter-spacing'] = intval( $letter_spacing ) . 'px';
			}
		}
		
		if ( ! empty( $text_transform ) ) {
			$styles['text-transform'] = $text_transform;
		}
		
		if ( ! empty( $margin_top ) ) {
			if ( qode_framework_string_ends_with( $margin_top, 'px' ) || qode_framework_string_ends_with( $margin_top, 'em' ) || qode_framework_string_ends_with( $margin_top, 'rem' ) ) {
				$styles['margin-top'] = $margin_top;
			} else {
				$styles['margin-top'] = intval( $margin_top ) . 'px';
			}
		}
		
		if ( ! empty( $margin_bottom ) ) {
			if ( qode_framework_string_ends_with( $margin_bottom, 'px' ) || qode_framework_string_ends_with( $margin_bottom, 'em' ) || qode_framework_string_ends_with( $margin_bottom, 'rem' ) ) {
				$styles['margin-bottom'] = $margin_bottom;
			} else {
				$styles['margin-bottom'] = intval( $margin_bottom ) . 'px';
			}
		}
		
		return $styles;
	}
}

if ( ! function_exists( 'boldlab_core_get_typography_hover_styles' ) ) {
	/**
	 * Generates hover typography styles
	 *
	 * @param $scope string
	 * @param $field_name string
	 *
	 * @return array
	 */
	function boldlab_core_get_typography_hover_styles( $scope, $field_name ) {
		$hover_color           = qode_framework_get_option_value( $scope, 'admin', $field_name . '_hover_color' );
		$hover_text_decoration = qode_framework_get_option_value( $scope, 'admin', $field_name . '_hover_text_decoration' );
		
		$styles = array();
		
		if ( ! empty( $hover_color ) ) {
			$styles['color'] = $hover_color;
		}
		
		if ( ! empty( $hover_text_decoration ) ) {
			$styles['text-decoration'] = $hover_text_decoration;
		}
		
		return $styles;
	}
}