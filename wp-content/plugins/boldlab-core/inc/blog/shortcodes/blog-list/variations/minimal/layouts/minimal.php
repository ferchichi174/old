<article <?php post_class( $item_classes ); ?>>
	<div class="qodef-e-inner">
		<div class="qodef-e-info qodef-info--top">
			<?php
			// Include post category info
			boldlab_core_theme_template_part( 'blog', 'templates/parts/post-info/category' );
			boldlab_core_template_part( 'blog/shortcodes/blog-list', 'templates/post-info/date' );
			?>
		</div>
		<?php boldlab_core_template_part( 'blog/shortcodes/blog-list', 'templates/post-info/title', '', $params ); ?>
		<div class="qodef-e-info qodef-info--bottom">
			<?php
			// Include post date info
			boldlab_core_theme_template_part( 'blog', 'templates/parts/post-info/read-more' );
			?>
		</div>
	</div>
</article>