(function ($) {

    $(document).ready(function () {
        qodefBlogListMinimal.init();
    });

    var qodefBlogListMinimal = {
        init: function () {
            var $sections = $('.qodef-blog.qodef-item-layout--minimal.qodef--with-animation');

            $sections.length && $sections.each(function () {
                var $section = $(this);
                qodefBlogListMinimal.observe($section);
            });
        },
        observe: function ($section) {
            var $items = $section.find('.qodef-e');

            $section.appear(function () {
                $items.each(function (i) {
                    var $item = $(this);

                    qodefBlogListMinimal.animate($item, i / 10);
                })
            })
        },
        animate: function ($item, delay) {
            var $top = $item.find('.qodef-info--top'),
                $title = $item.find('.qodef-e-title'),
                $bottom = $item.find('.qodef-info--bottom');

            var timeline = new TimelineMax();

            timeline
                .to($item, .6, {
                    autoAlpha: 1,
                    delay: Modernizr.touchevents ? 0 : delay
                })
                .from($top, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.6")
                .from($title, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4")
                .from($bottom, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4");
        }
    };

})(jQuery);