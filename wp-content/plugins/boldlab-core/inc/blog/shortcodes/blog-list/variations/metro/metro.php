<?php

if ( ! function_exists( 'boldlab_core_add_blog_list_variation_metro' ) ) {
	function boldlab_core_add_blog_list_variation_metro( $variations ) {
		$variations['metro'] = esc_html__( 'Metro', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_blog_list_layouts', 'boldlab_core_add_blog_list_variation_metro' );
}