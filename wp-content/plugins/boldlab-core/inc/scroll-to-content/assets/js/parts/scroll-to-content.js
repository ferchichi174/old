(function ($) {

    $(document).ready(function () {
        qodefSTC.init();
    });

    var qodefSTC = {
        data: [],
        DOM: [],
        init: function () {
            var revSliderLanding = $("#qodef-rev-slider-landing"); //unique ID for rev slider holder

            if (revSliderLanding.length && !Modernizr.touchevents && window.scrollY < window.innerHeight * 0.5) {
                qodefSTC.data.h = revSliderLanding.height();
                qodefSTC.data.offset = revSliderLanding.offset().top;
                qodefSTC.data.area = qodefSTC.data.h - qodefSTC.data.offset;
                qodefSTC.DOM.$revSlider = revSliderLanding.find('rs-module');
                qodefSTC.data.set = false;
                qodefSTC.data.done = false;

                qodefSTC.initEvents();
            }
        },
        scrollTo: function () {
            TweenMax.to(window, .8, {
                scrollTo: qodefSTC.data.area,
                ease: Quint.easeInOut,
                onStart: function() {
                    $(document).trigger('qodefScrolledTo');
                },
                onComplete: function () {
                    qodefSTC.data.done = true;
                }
            });
        },
        scrollHandler: function () {
            window.addEventListener('wheel', function (event) {
                if (!qodefSTC.data.done) {
                    event.preventDefault();
                    qodefSTC.scrollTo();
                }
            }, {
                passive: false
            });

            //scrollbar click
            $(document).on('mousedown', function (event) {
                if ($(window).outerWidth() <= event.pageX && !qodefSTC.data.done && $(window).scrollTop() == qodefSTC.data.offset) {
                    event.preventDefault();
                    qodefSTC.scrollTo();
                }
            });
        },
        initEvents: function () {
            //prevent mousewheel scroll
            window.addEventListener('wheel', function (event) {
                if (!qodefSTC.data.set) {
                    event.preventDefault();
                    window.scrollTop(0,0);
                }
            });

            //init
            qodefSTC.DOM.$revSlider.bind('revolution.slide.onloaded', function (e, data) {
                qodefSTC.data.set = true;
                qodefSTC.scrollHandler();
            });
        }
    };

})(jQuery);