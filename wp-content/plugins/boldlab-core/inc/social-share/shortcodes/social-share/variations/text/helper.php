<?php

if ( ! function_exists( 'boldlab_core_add_social_share_variation_text' ) ) {
	function boldlab_core_add_social_share_variation_text( $variations ) {
		
		$variations['text'] = esc_html__( 'Text', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_social_share_layouts', 'boldlab_core_add_social_share_variation_text' );
}
