<?php

if ( ! function_exists( 'boldlab_core_add_social_share_variation_dropdown' ) ) {
	function boldlab_core_add_social_share_variation_dropdown( $variations ) {
		
		$variations['dropdown'] = esc_html__( 'Dropdown', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_social_share_layouts', 'boldlab_core_add_social_share_variation_dropdown' );
}
