<?php

if ( ! function_exists( 'boldlab_core_add_general_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function boldlab_core_add_general_options( $page ) {
		
		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'    => 'color',
					'name'          => 'qodef_main_color',
					'title'         => esc_html__( 'Main Color', 'boldlab-core' ),
					'description'   => esc_html__( 'Choose the most dominant theme color', 'boldlab-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'color',
					'name'          => 'qodef_page_background_color',
					'title'         => esc_html__( 'Page Background Color', 'boldlab-core' ),
					'description'   => esc_html__( 'Set a background color for this website', 'boldlab-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_page_background_image',
					'title'       => esc_html__( 'Page Background Image', 'boldlab-core' ),
					'description' => esc_html__( 'Set Background Image for Website', 'boldlab-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_page_background_repeat',
					'title'         => esc_html__( 'Page Background Repeat', 'boldlab-core' ),
					'description'   => esc_html__( 'Set Background Repeat for Website', 'boldlab-core' ),
					'options'       => array(
						''          => esc_html__( 'Default', 'boldlab-core' ),
						'no-repeat' => esc_html__( 'No Repeat', 'boldlab-core' ),
						'repeat'    => esc_html__( 'Repeat', 'boldlab-core' ),
						'repeat-x'  => esc_html__( 'Repeat-x', 'boldlab-core' ),
						'repeat-y'  => esc_html__( 'Repeat-y', 'boldlab-core' )
					)
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_page_background_size',
					'title'         => esc_html__( 'Page Background Size', 'boldlab-core' ),
					'description'   => esc_html__( 'Set Background Size for Website', 'boldlab-core' ),
					'options'       => array(
						''        => esc_html__( 'Default', 'boldlab-core' ),
						'contain' => esc_html__( 'Contain', 'boldlab-core' ),
						'cover'   => esc_html__( 'Cover', 'boldlab-core' )
					)
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_page_background_attachment',
					'title'         => esc_html__( 'Page Background Attachment', 'boldlab-core' ),
					'description'   => esc_html__( 'Set Background Attachment for Website', 'boldlab-core' ),
					'options'       => array(
						''       => esc_html__( 'Default', 'boldlab-core' ),
						'fixed'  => esc_html__( 'Fixed', 'boldlab-core' ),
						'scroll' => esc_html__( 'Scroll', 'boldlab-core' )
					)
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'text',
					'name'          => 'qodef_page_content_padding',
					'title'         => esc_html__( 'Page Content Padding', 'boldlab-core' ),
					'description'   => esc_html__( 'Set padding that will be applied for page content in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'boldlab-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'text',
					'name'          => 'qodef_page_content_padding_mobile',
					'title'         => esc_html__( 'Page Content Padding Mobile', 'boldlab-core' ),
					'description'   => esc_html__( 'Set padding that will be applied for page content on mobile screens (1024px and below) in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'boldlab-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_boxed',
					'title'         => esc_html__( 'Boxed Layout', 'boldlab-core' ),
					'description'   => esc_html__( 'Set Boxed Layout', 'boldlab-core' ),
					'default_value' => 'no'
				)
			);
			
			$boxed_section = $page->add_section_element(
				array(
					'name'       => 'qodef_boxed_section',
					'title'      => esc_html__( 'Boxed Layout Section', 'boldlab-core' ),
					'dependency' => array(
						'hide' => array(
							'qodef_boxed' => array(
								'values'        => 'no',
								'default_value' => ''
							)
						)
					)
				)
			);
			
			$boxed_section->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_boxed_background_color',
					'title'       => esc_html__( 'Boxed Background Color', 'boldlab-core' ),
					'description' => esc_html__( 'Set Boxed Background Color', 'boldlab-core' )
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_content_width',
					'title'         => esc_html__( 'Initial Width of Content', 'boldlab-core' ),
					'description'   => esc_html__( 'Choose the initial width of content which is in grid (Applies to pages set to "Default Template" and rows set to "In Grid")', 'boldlab-core' ),
					'options'       => boldlab_core_get_select_type_options_pool( 'content_width', false ),
					'default_value' => '1300'
				)
			);
			
			// Hook to include additional options after module options
			do_action( 'boldlab_core_action_after_general_options_map', $page );
			
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'reset_to_default',
					'title'         => esc_html__( 'Reset options', 'boldlab-core' ),
					'description'   => esc_html__( 'If set to yes, options will be reset to default', 'boldlab-core' ),
					'default_value' => 'no'
				)
			);
		}
	}
	
	add_action( 'boldlab_core_action_default_options_init', 'boldlab_core_add_general_options' );
}