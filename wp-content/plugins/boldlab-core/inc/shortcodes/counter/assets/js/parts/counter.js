(function ($) {
	'use strict';
	
	$(document).ready(function () {
		qodefCounter.init();
	});
	
	/**
	 * Init counter functionality
	 */
	var qodefCounter = {
		init: function () {
			this.counters = $('.qodef-counter');
			
			if (this.counters.length) {
				this.counters.each(function () {
					var $thisCounter = $(this),
						counterElement = $thisCounter.find('.qodef-m-digit'),
						options = qodefCounter.generateOptions($thisCounter);

					$thisCounter.appear(function () {
						qodefCounter.counterScript(counterElement, options);
					});
				});
			}
		},
		generateOptions: function(counter) {
			var options = {};
			options.start = counter.data('start-digit') !== undefined ? counter.data('start-digit') : 0;
			options.end = counter.data('end-digit') !== undefined ? counter.data('end-digit') : null;
			options.duration = counter.data('duration') !== undefined ? counter.data('duration') : 1500;
			options.text = counter.data('digit-label') !== undefined ? counter.data('digit-label') : '';
			
			options.start = parseFloat(options.start);
			options.end = parseFloat(options.end);
			options.duration = parseFloat(options.duration);
			return options;
		},
		counterScript: function (counterElement, options) {
			counterElement.countTo({
				speed: options.duration,
				from: options.start,
				to: options.end,
				label: options.text,
				separator: '',
				thousand: '',
				refreshInterval: 40,
				onUpdate: function() {
					
				},
				onComplete: function() {
					
				}
			});
		}
	};
	
})(jQuery);