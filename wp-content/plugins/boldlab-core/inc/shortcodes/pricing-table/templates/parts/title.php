<?php if ( ! empty( $title ) ) { ?>
	<div class="qodef-m-title">
		<h6 <?php qode_framework_inline_style( $title_styles ); ?>><?php echo esc_html( $title ); ?></h6>
	</div>
<?php } ?>