<?php

if ( ! function_exists( 'boldlab_core_add_button_variation_textual' ) ) {
	function boldlab_core_add_button_variation_textual( $variations ) {
		
		$variations['textual'] = esc_html__( 'Textual', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_button_layouts', 'boldlab_core_add_button_variation_textual' );
}
