<?php

if ( ! function_exists( 'boldlab_core_add_button_variation_outlined' ) ) {
	function boldlab_core_add_button_variation_outlined( $variations ) {
		
		$variations['outlined'] = esc_html__( 'Outlined', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_button_layouts', 'boldlab_core_add_button_variation_outlined' );
}
