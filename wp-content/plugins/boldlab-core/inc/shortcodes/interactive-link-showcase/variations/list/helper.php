<?php

if ( ! function_exists( 'boldlab_core_add_interactive_link_showcase_variation_list' ) ) {
	function boldlab_core_add_interactive_link_showcase_variation_list( $variations ) {
		
		$variations['list'] = esc_html__( 'List', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_interactive_link_showcase_layouts', 'boldlab_core_add_interactive_link_showcase_variation_list' );
}
