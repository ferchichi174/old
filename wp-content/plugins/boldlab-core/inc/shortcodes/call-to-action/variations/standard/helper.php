<?php

if ( ! function_exists( 'boldlab_core_add_call_to_action_variation_standard' ) ) {
	function boldlab_core_add_call_to_action_variation_standard( $variations ) {
		
		$variations['standard'] = esc_html__( 'Standard', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_call_to_action_layouts', 'boldlab_core_add_call_to_action_variation_standard' );
}
