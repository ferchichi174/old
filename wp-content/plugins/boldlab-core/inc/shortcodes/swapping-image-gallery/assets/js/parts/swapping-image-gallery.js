(function ($) {
	'use strict';

	$(document).ready(function () {
		qodefSwappingImageGallery.init();
	});

	/**
	 * Init progress bar shortcode functionality
	 */
	var qodefSwappingImageGallery = {
		init: function () {
			this.holder = $('.qodef-swapping-image-gallery');

			if (this.holder.length) {
				this.holder.each(function () {
					var $thisHolder = $(this);

					qodefSwappingImageGallery.createSlider($thisHolder);
					qodefSwappingImageGallery.observe($thisHolder);
				});
			}
		},
		observe: function($holder) {
			var $thumbnails = qodefSwappingImageGallery.getItems($holder);

			$holder.appear(function () {
				qodefSwappingImageGallery.loadingAnimation($thumbnails);
			});
		},
		loadingAnimation: function ($thumbnails) {
			TweenMax.staggerFromTo($thumbnails, .4, {
				autoAlpha: 0,
				y: 100,
				scaleY: 1.8,
				skewY: "20deg",
				transformOrigin: "top",
			}, {
				autoAlpha: 1,
				y: 0,
				scaleY: 1,
				skewY: 0,
				ease: Power4.easeInOut
			}, .1);
		},
        getItems: function ($holder) {
            var $items = $holder.find('.qodef-m-thumbnail').sort(function (a, b) {
                return 0.5 - Math.random()
            })
            return $items;
        },
		createSlider: function (holder) {
			var swiperHolder = holder.find('.qodef-m-image-holder');
			var paginationHolder = holder.find('.qodef-m-thumbnails-holder .qodef-grid-inner');
			var spaceBetween = 0;
			var slidesPerView = 1;
			var centeredSlides = false;
			var loop = false;
			var autoplay = false;
			var speed = 800;

			var $swiper = new Swiper(swiperHolder, {
				slidesPerView: slidesPerView,
				centeredSlides: centeredSlides,
				spaceBetween: spaceBetween,
				autoplay: autoplay,
				loop: loop,
				speed: speed,
				pagination: {
					el: paginationHolder,
					type: 'custom',
					clickable: true,
					bulletClass: 'qodef-m-thumbnail'
				},
				on: {
					init: function () {
						swiperHolder.addClass('qodef-swiper--initialized');
						paginationHolder.find('.qodef-m-thumbnail').eq(0).addClass('qodef--active');
					},
					slideChange: function slideChange() {
						var swiper = this;
						var activeIndex = swiper.activeIndex;
						paginationHolder.find('.qodef--active').removeClass('qodef--active');
						paginationHolder.find('.qodef-m-thumbnail').eq(activeIndex).addClass('qodef--active');
					}
				}
			});
		}
	};

})(jQuery);