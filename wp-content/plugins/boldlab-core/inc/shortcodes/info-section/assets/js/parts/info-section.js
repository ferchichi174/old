(function ($) {

    $(document).ready(function () {
        qodefInfoSection.init();
    });

    var qodefInfoSection = {
        init: function () {
            var $sections = $('.qodef-info-section.qodef--with-animation');

            $sections.length && $sections.each(function () {
                var $section = $(this);

                qodefInfoSection.observe($section);
            });
        },
        observe: function ($section) {
            $section.appear(function () {
                qodefInfoSection.animate($section);
            })
        },
        animate: function ($section) {
            var delay = Modernizr.touchevents ? 0 : $section.data('delay'),
                $title = $section.find('.qodef-m-title'),
                $text = $section.find('.qodef-m-text'),
                $button = $section.find('.qodef-m-button'),
                $bgText = $section.find('.qodef-m-background-text-text');

            var timeline = new TimelineMax();

            timeline
                .to($section, .6, {
                    autoAlpha: 1,
                    delay: delay / 1000
                })
                .from($title, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.6")
                .from($text, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4")
                .from($button, .4, {
                    autoAlpha: 0,
                    y: 100,
                    scaleY: 1.2,
                    skewY: "10deg",
                    transformOrigin: "top",
                    ease: Power2.easeOut
                }, "-=.4")
                .from($bgText, 1.2, {
                    scale: 1.1,
                    autoAlpha: 0,
                    ease: Power2.easeOut
                }, "-=.3")

        }
    };

})(jQuery);