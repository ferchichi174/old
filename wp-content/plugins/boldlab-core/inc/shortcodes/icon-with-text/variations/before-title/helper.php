<?php

if ( ! function_exists( 'boldlab_core_add_icon_with_text_variation_before_title' ) ) {
	function boldlab_core_add_icon_with_text_variation_before_title( $variations ) {
		
		$variations['before-title'] = esc_html__( 'Before Title', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_icon_with_text_layouts', 'boldlab_core_add_icon_with_text_variation_before_title' );
}
