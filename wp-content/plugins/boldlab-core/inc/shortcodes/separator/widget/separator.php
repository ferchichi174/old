<?php

if ( ! function_exists( 'boldlab_core_add_separator_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param $widgets array
	 *
	 * @return array
	 */
	function boldlab_core_add_separator_widget( $widgets ) {
		$widgets[] = 'BoldlabCoreSeparatorWidget';
		
		return $widgets;
	}
	
	add_filter( 'boldlab_core_filter_register_widgets', 'boldlab_core_add_separator_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class BoldlabCoreSeparatorWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$widget_mapped = $this->import_shortcode_options( array(
				'shortcode_base' => 'boldlab_core_separator'
			) );
			if( $widget_mapped ) {
				$this->set_base( 'boldlab_core_separator' );
				$this->set_name( esc_html__( 'Boldlab Separator', 'boldlab-core' ) );
				$this->set_description( esc_html__( 'Add a separator element into widget areas', 'boldlab-core' ) );
			}
		}
		
		public function render( $atts ) {
			$params = $this->generate_string_params( $atts );
			
			echo do_shortcode( "[boldlab_core_separator $params]" ); // XSS OK
		}
	}
}