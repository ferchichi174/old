<?php

if ( ! function_exists( 'boldlab_core_add_standard_header_meta' ) ) {
	function boldlab_core_add_standard_header_meta( $page ) {
		
		$section = $page->add_section_element(
			array(
				'name'       => 'qodef_standard_header_section',
				'title'      => esc_html__( 'Standard Header', 'boldlab-core' ),
				'dependency' => array(
					'show' => array(
						'qodef_header_layout' => array(
							'values' => 'standard',
							'default_value' => ''
						)
					)
				)
			)
		);

		$section->add_field_element(
			array(
				'field_type'    => 'select',
				'name'          => 'qodef_standard_header_in_grid',
				'title'         => esc_html__( 'Content in Grid', 'boldlab-core' ),
				'description'   => esc_html__( 'Set content to be in grid', 'boldlab-core' ),
				'default_value' => '',
				'options'       => boldlab_core_get_select_type_options_pool( 'no_yes' )
			)
		);
		
		$section->add_field_element(
			array(
				'field_type'  => 'text',
				'name'        => 'qodef_standard_header_height',
				'title'       => esc_html__( 'Header Height', 'boldlab-core' ),
				'description' => esc_html__( 'Enter header height', 'boldlab-core' ),
				'args'        => array(
					'suffix' => 'px'
				)
			)
		);
		
		$section->add_field_element(
			array(
				'field_type'  => 'color',
				'name'        => 'qodef_standard_header_background_color',
				'title'       => esc_html__( 'Header Background Color', 'boldlab-core' ),
				'description' => esc_html__( 'Enter header background color', 'boldlab-core' )
			)
		);
		
		$section->add_field_element(
			array(
				'field_type'    => 'select',
				'name'          => 'qodef_standard_header_menu_position',
				'title'         => esc_html__( 'Menu position', 'boldlab-core' ),
				'default_value' => 'right',
				'options'       => array(
					''       => esc_html__( 'Default', 'boldlab-core' ),
					'left'   => esc_html__( 'Left', 'boldlab-core' ),
					'center' => esc_html__( 'Center', 'boldlab-core' ),
					'right'  => esc_html__( 'Right', 'boldlab-core' ),
				)
			)
		);
	}
	
	add_action( 'boldlab_core_action_after_page_header_meta_map', 'boldlab_core_add_standard_header_meta' );
}