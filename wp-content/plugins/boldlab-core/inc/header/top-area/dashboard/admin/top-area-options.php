<?php

if ( ! function_exists( 'boldlab_core_add_top_area_options' ) ) {
	function boldlab_core_add_top_area_options( $page ) {

		$top_area_section = $page->add_section_element(
			array(
				'name'       => 'qodef_top_area_section',
				'title'      => esc_html__( 'Top Area', 'boldlab-core' ),
				'dependency' => array(
					'hide' => array(
						'qodef_header_layout' => array(
							'values' => boldlab_core_dependency_for_top_area_options(),
							'default_value' => 'standard'
						)
					)
				)
			)
		);

			$top_area_section->add_field_element(
				array(
					'field_type'  => 'yesno',
					'default_value' => 'no',
					'name'        => 'qodef_top_area_header',
					'title'       => esc_html__( 'Top Area', 'boldlab-core' ),
					'description' => esc_html__( 'Enable top area', 'boldlab-core' )
				)
			);

			$top_area_options_section = $top_area_section->add_section_element(
				array(
					'name'       => 'qodef_top_area_options_section',
					'title'       => esc_html__( 'Top Area', 'boldlab-core' ),
					'dependency' => array(
						'show' => array(
							'qodef_top_area_header' => array(
								'values' => 'yes',
								'default_value' => 'no'
							)
						)
					)
				)
			);

				$top_area_options_section->add_field_element(
					array(
						'field_type'  => 'color',
						'name'        => 'qodef_top_area_header_background_color',
						'title'       => esc_html__( 'Top Area Background Color', 'boldlab-core' ),
						'description' => esc_html__( 'Choose top area background color', 'boldlab-core' )
					)
				);

				$top_area_options_section->add_field_element(
					array(
						'field_type'  => 'text',
						'name'        => 'qodef_top_area_header_height',
						'title'       => esc_html__( 'Top Area Height', 'boldlab-core' ),
						'description' => esc_html__( 'Enter top area height (Default is 50px)', 'boldlab-core' ),
						'args'        => array(
							'suffix'    => 'px'
						)
					)
				);

				$top_area_options_section->add_field_element(
					array(
						'field_type'  => 'text',
						'name'        => 'qodef_top_area_header_side_padding',
						'title'       => esc_html__( 'Top Area Side Padding', 'boldlab-core' ),
						'args'        => array(
							'suffix'    => esc_html__( 'px or %', 'boldlab-core' )
						)
					)
				);
	}
	
	add_action( 'boldlab_core_action_after_header_options_map', 'boldlab_core_add_top_area_options');
}