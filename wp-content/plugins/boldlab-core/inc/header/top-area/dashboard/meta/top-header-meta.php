<?php
if ( ! function_exists( 'boldlab_core_add_top_area_meta_options' ) ) {
	function boldlab_core_add_top_area_meta_options($page){
		$top_area_section = $page->add_section_element(
			array(
				'name' => 'qodef_top_area_section',
				'title' => esc_html__('Top Area', 'boldlab-core'),
				'dependency' => array(
					'hide' => array(
						'qodef_header_layout' => array(
							'values' => boldlab_core_dependency_for_top_area_options(),
							'default_value' => ''
						)
					)
				)
			)
		);
		
		$top_area_section->add_field_element(
			array(
				'field_type'  => 'select',
				'name'        => 'qodef_top_area_header',
				'title'       => esc_html__( 'Top Area', 'boldlab-core' ),
				'description' => esc_html__( 'Enable Top Area', 'boldlab-core' ),
				'options'     => boldlab_core_get_select_type_options_pool( 'yes_no' )
			)
		);
	}
	
	add_action( 'boldlab_core_action_after_page_header_meta_map', 'boldlab_core_add_top_area_meta_options' );
}