<?php

if ( ! function_exists( 'boldlab_core_add_page_logo_meta_box' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function boldlab_core_add_page_logo_meta_box( $page ) {
		
		if ( $page ) {
			
			$logo_tab = $page->add_tab_element(
				array(
					'name'        => 'tab-logo',
					'icon'        => 'fa fa-cog',
					'title'       => esc_html__( 'Logo Settings', 'boldlab-core' ),
					'description' => esc_html__( 'Logo settings', 'boldlab-core' )
				)
			);
			
			$header_logo_section = $logo_tab->add_section_element(
				array(
					'name'       => 'qodef_header_logo_section',
					'title'      => esc_html__( 'Header Logo Options', 'boldlab-core' ),
				)
			);
			
			$header_logo_section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_logo_height',
					'title'       => esc_html__( 'Logo Height', 'boldlab-core' ),
					'description' => esc_html__( 'Enter Logo Height', 'boldlab-core' ),
					'args'        => array(
						'suffix' => 'px'
					)
				)
			);
			
			$header_logo_section->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_main',
					'title'       => esc_html__( 'Logo - Main', 'boldlab-core' ),
					'description' => esc_html__( 'Choose main logo image', 'boldlab-core' ),
					'multiple'    => 'no'
				)
			);
			
			$header_logo_section->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_dark',
					'title'       => esc_html__( 'Logo - Dark', 'boldlab-core' ),
					'description' => esc_html__( 'Choose dark logo image', 'boldlab-core' ),
					'multiple'    => 'no'
				)
			);
			
			$header_logo_section->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_light',
					'title'       => esc_html__( 'Logo - Light', 'boldlab-core' ),
					'description' => esc_html__( 'Choose light logo image', 'boldlab-core' ),
					'multiple'    => 'no'
				)
			);
			
			// Hook to include additional options after module options
			do_action( 'boldlab_core_action_after_page_logo_meta_map', $logo_tab );
		}
	}
	
	add_action( 'boldlab_core_action_after_general_meta_box_map', 'boldlab_core_add_page_logo_meta_box' );
}