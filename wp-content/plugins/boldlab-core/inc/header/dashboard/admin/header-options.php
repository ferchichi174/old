<?php

if ( ! function_exists( 'boldlab_core_add_header_options' ) ) {
	/**
	 * Function that add header options for this module
	 */
	function boldlab_core_add_header_options() {
		$qode_framework = qode_framework_get_framework_root();

		$page = $qode_framework->add_options_page(
			array(
				'scope'       => BOLDLAB_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'header',
				'icon'        => 'fa fa-cog',
				'title'       => esc_html__( 'Header', 'boldlab-core' ),
				'description' => esc_html__( 'Header Settings', 'boldlab-core' )
			)
		);

		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'    => 'radio',
					'name'          => 'qodef_header_layout',
					'title'         => esc_html__( 'Header Layout', 'boldlab-core' ),
					'description'   => esc_html__( 'Choose header layout to set for your site', 'boldlab-core' ),
					'args'          => array( 'images' => true ),
					'options'       => apply_filters( 'boldlab_core_filter_header_layout_option', $header_layout_options = array() ),
					'default_value' => apply_filters( 'boldlab_core_filter_header_layout_default_option_value', '' )
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_header_skin',
					'title'       => esc_html__( 'Header Skin', 'boldlab-core' ),
					'description' => esc_html__( 'Choose a predefined header style for header elements', 'boldlab-core' ),
					'options'     => array(
						'none'  => esc_html__( 'None', 'boldlab-core' ),
						'light' => esc_html__( 'Light', 'boldlab-core' ),
						'dark'  => esc_html__( 'Dark', 'boldlab-core' )
					)
				)
			);

			$page->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_header_scroll_appearance',
					'title'         => esc_html__( 'Header Scroll Appearance', 'boldlab-core' ),
					'description'   => esc_html__( 'Choose how header will act on scroll', 'boldlab-core' ),
					'options'       => apply_filters( 'boldlab_core_filter_header_scroll_appearance_option', $header_scroll_appearance_options = array( 'none' => esc_html__( 'None', 'boldlab-core' ) ) ),
					'default_value' => 'none',
				)
			);

			do_action( 'boldlab_core_action_after_header_options_map', $page );
		}
	}

	add_action( 'boldlab_core_action_default_options_init', 'boldlab_core_add_header_options', boldlab_core_get_admin_options_map_position( 'header' ) );
}