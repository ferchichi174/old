<?php

if ( ! function_exists( 'boldlab_core_add_admin_user_options' ) ) {
	function boldlab_core_add_admin_user_options() {
		$qode_framework = qode_framework_get_framework_root();
		
		$page = $qode_framework->add_options_page(
			array(
				'scope' => array( 'administrator' ),
				'type'  => 'user',
				'title' => esc_html__( 'Social Networks', 'boldlab-core' ),
				'slug'  => 'admin-options',
			)
		);
		
		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_user_facebook',
					'title'       => esc_html__( 'Facebook', 'boldlab-core' ),
					'description' => esc_html__( 'Enter user Instagram profile url', 'boldlab-core' ),
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_user_instagram',
					'title'       => esc_html__( 'Instagram', 'boldlab-core' ),
					'description' => esc_html__( 'Enter user Instagram profile url', 'boldlab-core' ),
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_user_twitter',
					'title'       => esc_html__( 'Twitter', 'boldlab-core' ),
					'description' => esc_html__( 'Enter user Twitter profile url', 'boldlab-core' ),
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_user_linkedin',
					'title'       => esc_html__( 'LinkedIn', 'boldlab-core' ),
					'description' => esc_html__( 'Enter user LinkedIn profile url', 'boldlab-core' ),
				)
			);
			
			$page->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_user_pinterest',
					'title'       => esc_html__( 'Pinterest', 'boldlab-core' ),
					'description' => esc_html__( 'Enter user Pinterest profile url', 'boldlab-core' ),
				)
			);
			
			// Hook to include additional options after module options
			do_action( 'boldlab_core_action_after_admin_user_options_map', $page );
		}
	}
	
	add_action( 'boldlab_core_action_register_role_custom_fields', 'boldlab_core_add_admin_user_options' );
}