<?php

if ( ! function_exists( 'boldlab_core_add_underscore_classes' ) ) {
	function boldlab_core_add_underscore_classes( $classes ) {
		$underscore = boldlab_core_get_post_value_through_levels( 'underscore_animation' );
		
		if ( $underscore == 'yes') {
			$classes[] = 'qodef--underscore';
		}
		
		return $classes;
	}
	
	add_filter( 'body_class', 'boldlab_core_add_underscore_classes' );
}