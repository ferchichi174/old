<article <?php post_class( 'qodef-team-list-item qodef-e' ); ?>>
	<div class="qodef-e-inner">
		<?php boldlab_core_template_part( 'post-types/team', 'templates/parts/post-info/image' ); ?>
		<div class="qodef-e-content">
			<?php boldlab_core_template_part( 'post-types/team', 'templates/parts/post-info/title' ); ?>
			<?php boldlab_core_template_part( 'post-types/team', 'templates/parts/post-info/role' ); ?>
			<?php boldlab_core_template_part( 'post-types/team', 'templates/parts/post-info/social-icons' ); ?>
			<?php boldlab_core_template_part( 'post-types/team', 'templates/parts/post-info/address' ); ?>
			<?php boldlab_core_template_part( 'post-types/team', 'templates/parts/post-info/birth-date' ); ?>
			<?php boldlab_core_template_part( 'post-types/team', 'templates/parts/post-info/education' ); ?>
			<?php boldlab_core_template_part( 'post-types/team', 'templates/parts/post-info/email' ); ?>
			<?php boldlab_core_template_part( 'post-types/team', 'templates/parts/post-info/resume' ); ?>
			<?php boldlab_core_template_part( 'post-types/team', 'templates/parts/post-info/excerpt' ); ?>
		</div>
	</div>
</article>