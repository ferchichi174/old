<?php

get_header();

// Include team content template
boldlab_core_template_part( 'post-types/team', 'templates/content' );

get_footer();