<?php

if ( ! function_exists( 'boldlab_core_add_testimonials_list_variation_info_below' ) ) {
	function boldlab_core_add_testimonials_list_variation_info_below( $variations ) {
		
		$variations['info-below'] = esc_html__( 'Info Below', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_testimonials_list_layouts', 'boldlab_core_add_testimonials_list_variation_info_below' );
}

if ( ! function_exists( 'boldlab_core_add_testimonials_list_options_info_below' ) ) {
	function boldlab_core_add_testimonials_list_options_info_below( $options ) {
		$info_below_options   = array();
		$margin_option        = array(
			'field_type' => 'text',
			'name'       => 'info_below_content_margin_top',
			'title'      => esc_html__( 'Content Top Margin', 'boldlab-core' ),
			'dependency' => array(
				'show' => array(
					'layout' => array(
						'values'        => 'info-below',
						'default_value' => 'default'
					)
				)
			),
			'group'      => esc_html__( 'Layout', 'boldlab-core' )
		);
		$info_below_options[] = $margin_option;
		
		return array_merge( $options, $info_below_options );
	}
	
	add_filter( 'boldlab_core_filter_testimonials_list_extra_options', 'boldlab_core_add_testimonials_list_options_info_below' );
}