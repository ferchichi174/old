(function ($) {
    'use strict';

    $(document).ready(function () {
        inverseFade.init();
    });

    /**
     * Init clients grid animation functionality
     */
    var inverseFade = {
        init: function () {
            this.holder = $('.qodef-clients-list.qodef-hover-animation--inverse-fade');
            if (this.holder.length) {
                this.holder.each(function () {
                    var $thisHolder = $(this),
                        $items = inverseFade.getItems($thisHolder);

                    !$thisHolder.hasClass('qodef-swiper-container') && $thisHolder.appear(function () {
                        inverseFade.loadingAnimation($items);
                    })

                    $items.each(function () {
                        var $item = $(this);
                        inverseFade.hoverAnimation($thisHolder, $item, $items);
                    });
                });
            }
        },
        loadingAnimation: function ($items) {
            TweenMax.staggerTo($items, .4, {
                autoAlpha: 1,
            }, .05);
        },
        hoverAnimation: function ($thisHolder, $item, $items) {
            $item
                .on('mouseenter', function (e) {
                    $item.addClass('qodef--hovered');
                    TweenMax.to($item, .2, {
                        autoAlpha: 1,
                        overwrite: 1
                    })
                    TweenMax.fromTo($item, .25, {
                        y: 0
                    }, {
                        y: -5,
                        ease: Power4.easeInOut,
                        yoyo: true,
                        repeat: 1,
                    })
                    TweenMax.staggerTo($items.not('.qodef--hovered'), .2, {
                        autoAlpha: .5,
                        overwrite: 1,
                        ease: Power4.easeOut,
                    }, .015);
                });

            $item
                .on('mouseleave', function () {
                    $item.removeClass('qodef--hovered');
                    TweenMax.staggerTo($items.filter('.qodef--hovered'), .2, {
                        autoAlpha: 0,
                        ease: Power2.easeOut
                    }, .1);
                });

            $thisHolder
                .on('mouseleave', function () {
                    $item.removeClass('qodef--hovered');
                    TweenMax.staggerTo($items, .2, {
                        autoAlpha: 1,
                        ease: Power2.easeOut
                    }, .05);
                });
        },
        getItems: function ($holder) {
            var $items = $holder.find('.qodef-e').sort(function (a, b) {
                return 0.5 - Math.random()
            })
            return $items;
        }
    };

})(jQuery);