<?php
if ( ! function_exists( 'boldlab_core_filter_clients_list_image_only_inverse_fade' ) ) {
	function boldlab_core_filter_clients_list_image_only_inverse_fade( $variations ) {
		
		$variations['inverse-fade'] = esc_html__( 'Inverse Fade', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_clients_list_image_only_animation_options', 'boldlab_core_filter_clients_list_image_only_inverse_fade', 5 );
}