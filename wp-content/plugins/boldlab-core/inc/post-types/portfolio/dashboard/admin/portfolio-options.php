<?php

if ( ! function_exists( 'boldlab_core_add_portfolio_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function boldlab_core_add_portfolio_options() {
		$qode_framework = qode_framework_get_framework_root();
		
		$page = $qode_framework->add_options_page(
			array(
				'scope'       => BOLDLAB_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'portfolio',
				'layout'      => 'tabbed',
				'icon'        => 'fa fa-cog',
				'title'       => esc_html__( 'Portfolio', 'boldlab-core' ),
				'description' => esc_html__( 'Global settings related to portfolio', 'boldlab-core' )
			)
		);
		
		if ( $page ) {
			$archive_tab = $page->add_tab_element(
				array(
					'name'        => 'tab-archive',
					'icon'        => 'fa fa-cog',
					'title'       => esc_html__( 'Archive Settings', 'boldlab-core' ),
					'description' => esc_html__( 'Settings related to portfolio archive pages', 'boldlab-core' )
				)
			);
			do_action( 'boldlab_core_action_after_portfolio_options_archive', $archive_tab );
			
			$single_tab = $page->add_tab_element(
				array(
					'name'        => 'tab-single',
					'icon'        => 'fa fa-cog',
					'title'       => esc_html__( 'Single Settings', 'boldlab-core' ),
					'description' => esc_html__( 'Settings related to portfolio single pages', 'boldlab-core' )
				)
			);
			$single_tab->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_portfolio_single_layout',
					'title'         => esc_html__( 'Single Layout', 'boldlab-core' ),
					'description'   => esc_html__( 'Choose default layout for portfolio single', 'boldlab-core' ),
					'default_value' => 'big-images',
					'options'       => apply_filters( 'boldlab_core_filter_portfolio_single_layout_options', array() )
				)
			);
			do_action( 'boldlab_core_action_after_portfolio_options_single', $single_tab );
			
			// Hook to include additional options after module options
			do_action( 'boldlab_core_action_after_portfolio_options_map', $page );
		}
	}
	
	add_action( 'boldlab_core_action_default_options_init', 'boldlab_core_add_portfolio_options', boldlab_core_get_admin_options_map_position( 'portfolio' ) );
}