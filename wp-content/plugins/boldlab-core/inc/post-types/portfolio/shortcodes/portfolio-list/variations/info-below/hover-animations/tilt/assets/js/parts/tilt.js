(function ($) {
	'use strict';
	
	$(window).on('load', function () {
		qodefTilt.init();
	});
	
	$(document).on('boldlab_trigger_get_new_posts', function () {
		qodefTilt.init();
	});
	
	/**
	 * Init tilt hover animation
	 */
	var qodefTilt = {
		init: function () {
            var $items = $('.qodef-hover-animation--tilt article');

            if ($items.length && !Modernizr.touchevents) {
                $items.each(function () {
                    var $trigger = $(this),
                        $targets = $trigger.find('.qodef-e-image');

                    $trigger
                        .on('mousemove touchmove', function (e) {
                            qodefTilt.enter(e, $trigger, $targets);
                        })
                        .on('mouseout touchend', function (e) {
                            qodefTilt.leave($targets);
                        });
                });
            }
        },
        enter: function (e, tr, ta) {
            var aFx = 70,
                trF = 4,
                cH = tr.innerHeight(),
                cW = tr.innerWidth(),
                eX = (e.originalEvent.type === 'touchmove') ? e.originalEvent.touches[0].pageX : e.offsetX,
				eY = (e.originalEvent.type === 'touchmove') ? e.originalEvent.touches[0].pageY : e.offsetY;
				
            $.each(ta, function (i, el) {
                TweenMax.set($(el), {
                    transformOrigin: ((eX / (cW * trF) / 100 * 10000) + (trF * 10)) + '% ' + ((eY / (cH * trF) / 100 * 10000) + (trF * 10)) + '%',
                    transformPerspective: 1000 + (i * 500)
                });
                TweenMax.to($(el), 0.5, {
                    rotationX: ((eY - cH / 2) / aFx) - i * 2,
                    rotationY: ((eX - cW / 2) / aFx * -1) - i * 2,
                    y: (eY - (cH / 2)) / (50 - i * 20),
                    x: (eX - (cW / 2)) / (50 - i * 20)
                });
            });
        },
        leave: function (ta) {
            $.each(ta, function (i, el) {
                TweenMax.to($(el), .75, {
                    delay: .2,
                    y: 0,
                    x: 0,
                    rotationX: 0,
                    rotationY: 0,
                    transformPerspective: '1500'
                });
            });
        },
	};
	
})(jQuery);