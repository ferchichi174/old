<?php

if ( ! function_exists( 'boldlab_core_add_portfolio_list_variation_info_follow' ) ) {
	function boldlab_core_add_portfolio_list_variation_info_follow( $variations ) {
		
		$variations['info-follow'] = esc_html__( 'Info Follow', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_portfolio_list_layouts', 'boldlab_core_add_portfolio_list_variation_info_follow' );
}