<?php
if ( ! function_exists( 'boldlab_core_filter_portfolio_list_info_follow' ) ) {
	function boldlab_core_filter_portfolio_list_info_follow( $variations ) {

		$variations['follow'] = esc_html__( 'Follow', 'boldlab-core' );

		return $variations;
	}

	add_filter( 'boldlab_core_filter_portfolio_list_info_follow_animation_options', 'boldlab_core_filter_portfolio_list_info_follow' );
}