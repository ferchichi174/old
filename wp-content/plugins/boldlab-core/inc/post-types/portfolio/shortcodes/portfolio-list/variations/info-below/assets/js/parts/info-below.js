(function ($) {

    $(window).on('load', function () {
        qodefInfoBelowParallax.init();
    });

    var qodefInfoBelowParallax = {
        init: function () {
            var $articles = $('.qodef-item-layout--info-below.qodef--with-parallax article');

            if ($articles.length && !Modernizr.touchevents) {
                $articles.each(function () {
                    var $article = $(this);

                    $article.data('y', Math.floor(Math.random() * 100) + 50);
                    qodefInfoBelowParallax.observe($article);
                })
            }
        },
        observe: function ($item) {
            qodefInfoBelowParallax.isInViewport($item[0]) && qodefInfoBelowParallax.animate($item);

            requestAnimationFrame(function () {
                qodefInfoBelowParallax.observe($item);
            })
        },
        animate: function ($item) {
            var y = qodefInfoBelowParallax.yVal($item);

            TweenMax.to($item.find('.qodef-e-inner'), 1, {
                y: y,
                ease: Power4.easeOut,
                overwrite: 1
            });
        },
        yVal: function ($el) {
            var max = 1;
            var min = 0;
            var y = ($el[0].getBoundingClientRect().top + $el[0].clientHeight) / window.innerHeight;
            return (1 - Math.max(Math.min(y, max), min)) * -$el.data('y');
        },
        isInViewport: function (elem) {
            var bounding = elem.getBoundingClientRect();
            return (
                bounding.top + elem.clientHeight >= 0 &&
                bounding.bottom - elem.clientHeight <= (window.innerHeight || document.documentElement.clientHeight)
            );
        }
    };

})(jQuery);