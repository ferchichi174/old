<?php
$portfolio_list_image = get_post_meta( get_the_ID(), 'qodef_portfolio_list_image', true );
$has_image            = ! empty ( $portfolio_list_image ) || has_post_thumbnail();
//$image_dimension      = ! empty( $image_dimension ) ? $image_dimension : 'full';
$image_dimension     = isset( $image_dimension ) && ! empty( $image_dimension ) ? esc_attr( $image_dimension['size'] ) : 'full';
//var_dump( $image_dimension );

$margin = '';
if ( ! empty( $info_below_content_margin_top ) ) {
	$margin = 'margin-bottom:' . ( qode_framework_string_ends_with( $info_below_content_margin_top, 'px' ) ? $info_below_content_margin_top : $info_below_content_margin_top . 'px' );
}
if ( $has_image ) { ?>
    <div class="qodef-e-media-image" <?php qode_framework_inline_style( $margin ); ?>>
        <a itemprop="url" href="<?php the_permalink(); ?>">
			<?php if ( ! empty ( $portfolio_list_image ) ) {
				echo wp_get_attachment_image( $portfolio_list_image, $image_dimension );
			} else {
				the_post_thumbnail( $image_dimension );
			} ?>
        </a>
    </div>
<?php } ?>