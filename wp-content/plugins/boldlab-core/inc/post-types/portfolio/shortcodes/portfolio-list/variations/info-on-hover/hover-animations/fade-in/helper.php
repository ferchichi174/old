<?php

if ( ! function_exists( 'boldlab_core_filter_portfolio_list_info_on_hover_fade_in' ) ) {
	function boldlab_core_filter_portfolio_list_info_on_hover_fade_in( $variations ) {
		$variations['fade-in'] = esc_html__( 'Fade In', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_portfolio_list_info_on_hover_animation_options', 'boldlab_core_filter_portfolio_list_info_on_hover_fade_in' );
}