(function ($) {
	'use strict';
	
	$(document).ready(function () {
		qodefHoverDir.init();
	});
	
	$(document).on('boldlab_trigger_get_new_posts', function () {
		qodefHoverDir.init();
	});
	
	/**
	 * Init hoverdir hover animation
	 */
	var qodefHoverDir = {
		init: function () {
			var $gallery = $('.qodef-hover-animation--direction-aware');
			
			if ($gallery.length) {
				$gallery.each(function () {
					var $this = $(this);
					$this.find('article').each(function () {
						$(this).hoverdir({
							hoverElem: 'div.qodef-e-content',
							speed: 330,
							hoverDelay: 35,
							easing: 'ease'
						});
					});
				});
			}
		}
	};
	
})(jQuery);