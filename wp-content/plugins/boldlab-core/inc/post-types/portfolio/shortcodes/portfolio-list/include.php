<?php

include_once 'portfolio-list.php';

foreach ( glob( BOLDLAB_CORE_INC_PATH . '/post-types/portfolio/shortcodes/portfolio-list/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}