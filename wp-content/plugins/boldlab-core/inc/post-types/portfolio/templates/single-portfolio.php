<?php

get_header();

// Include portfolio content template
boldlab_core_template_part( 'post-types/portfolio', 'templates/content' );

get_footer();