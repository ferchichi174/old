<?php

if ( ! function_exists( 'boldlab_core_register_portfolio_for_meta_options' ) ) {
	function boldlab_core_register_portfolio_for_meta_options( $post_types ) {
		$post_types[] = 'portfolio';

		return $post_types;
	}

	add_filter( 'qode_framework_filter_meta_box_save', 'boldlab_core_register_portfolio_for_meta_options' );
	add_filter( 'qode_framework_filter_meta_box_remove', 'boldlab_core_register_portfolio_for_meta_options' );
}

if ( ! function_exists( 'boldlab_core_add_portfolio_custom_post_type' ) ) {
	/**
	 * Function that adds portfolio custom post type
	 *
	 * @param $cpts array
	 *
	 * @return array
	 */
	function boldlab_core_add_portfolio_custom_post_type( $cpts ) {
		$cpts[] = 'BoldlabCorePortfolioCPT';

		return $cpts;
	}

	add_filter( 'boldlab_core_filter_register_custom_post_types', 'boldlab_core_add_portfolio_custom_post_type' );
}

if ( class_exists( 'QodeFrameworkCustomPostType' ) ) {
	class BoldlabCorePortfolioCPT extends QodeFrameworkCustomPostType {

		public function map_post_type() {
			$name = esc_html__( 'Portfolio', 'boldlab-core' );
			$this->set_base( 'portfolio' );
			$this->set_menu_position( 10 );
			$this->set_menu_icon( 'dashicons-screenoptions' );
			$this->set_slug( 'portfolio' );
			$this->set_name( $name );
			$this->set_path( BOLDLAB_CORE_CPT_PATH . '/portfolio' );
			$this->set_labels( array(
				'name'          => esc_html__( 'Boldlab Portfolio', 'boldlab-core' ),
				'singular_name' => esc_html__( 'Portfolio Item', 'boldlab-core' ),
				'add_item'      => esc_html__( 'New Portfolio Item', 'boldlab-core' ),
				'add_new_item'  => esc_html__( 'Add New Portfolio Item', 'boldlab-core' ),
				'edit_item'     => esc_html__( 'Edit Portfolio Item', 'boldlab-core' )
			) );
			$this->add_post_taxonomy( array(
				'base'          => 'portfolio-category',
				'slug'          => 'portfolio-category',
				'singular_name' => esc_html__( 'Category', 'boldlab-core' ),
				'plural_name'   => esc_html__( 'Categories', 'boldlab-core' ),
			) );
		}

	}
}