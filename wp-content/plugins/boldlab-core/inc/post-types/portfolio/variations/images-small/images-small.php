<?php

if ( ! function_exists( 'boldlab_core_add_portfolio_single_variation_images_small' ) ) {
	function boldlab_core_add_portfolio_single_variation_images_small( $variations ) {
		
		$variations['images-small'] = esc_html__( 'Images - Small', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_portfolio_single_layout_options', 'boldlab_core_add_portfolio_single_variation_images_small' );
}