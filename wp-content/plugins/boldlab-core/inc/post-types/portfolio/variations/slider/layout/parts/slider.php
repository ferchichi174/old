<div class="qodef-media qodef-postfolio-single-slider qodef-swiper-container" data-slider-options='{"slidesPerView":"auto", "centeredSlides":"true", "slidesPerView1440":"auto", "slidesPerView1366":"auto", "slidesPerView1024":"auto", "slidesPerView768":"auto", "slidesPerView680":"auto", "slidesPerView480":"auto"}'>
    <div class="swiper-wrapper">
		<?php boldlab_core_template_part( 'post-types/portfolio', 'templates/parts/post-info/media', 'slider'); ?>
    </div>
    <div class="swiper-pagination"></div>
</div>