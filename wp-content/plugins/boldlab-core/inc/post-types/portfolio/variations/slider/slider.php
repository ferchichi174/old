<?php

if ( ! function_exists( 'boldlab_core_add_portfolio_single_variation_slider' ) ) {
	function boldlab_core_add_portfolio_single_variation_slider( $variations ) {
		
		$variations['slider'] = esc_html__( 'Slider', 'boldlab-core' );
		
		return $variations;
	}
	
	add_filter( 'boldlab_core_filter_portfolio_single_layout_options', 'boldlab_core_add_portfolio_single_variation_slider' );
}

if ( ! function_exists( 'boldlab_core_add_portfolio_single_slider' ) ) {
	function boldlab_core_add_portfolio_single_slider() {
		if(boldlab_core_get_post_value_through_levels('qodef_portfolio_single_layout') == 'slider') {
			boldlab_core_template_part('post-types/portfolio', 'variations/slider/layout/parts/slider');
		}

	}

	add_action( 'boldlab_action_before_page_inner', 'boldlab_core_add_portfolio_single_slider' );
}