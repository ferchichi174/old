<?php
include_once BOLDLAB_CORE_INC_PATH . '/core-dashboard/core-dashboard.php';

if ( ! function_exists( 'boldlab_core_dashboard_load_files' ) ) {
	function boldlab_core_dashboard_load_files() {
		include_once BOLDLAB_CORE_INC_PATH . '/core-dashboard/rest/include.php';
		include_once BOLDLAB_CORE_INC_PATH . '/core-dashboard/registration-rest.php';
		include_once BOLDLAB_CORE_INC_PATH . '/core-dashboard/sub-pages/sub-page.php';

		foreach ( glob( BOLDLAB_CORE_INC_PATH . '/core-dashboard/sub-pages/*/load.php' ) as $subpages ) {
			include_once $subpages;
		}
	}

	add_action( 'after_setup_theme', 'boldlab_core_dashboard_load_files' );
}