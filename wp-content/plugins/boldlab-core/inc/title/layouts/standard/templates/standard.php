<?php
// Load title image template
boldlab_core_get_page_title_image(); ?>
<div class="qodef-m-content <?php echo esc_attr( boldlab_core_get_page_title_content_classes() ); ?>">
	<h1 class="qodef-m-title entry-title <?php echo esc_attr( boldlab_core_get_page_title_tag_classes() ); ?>">
		<?php if ( qode_framework_is_installed( 'theme' ) ) {
			echo esc_html( boldlab_get_page_title_text() );
		} else {
			echo get_option( 'blogname' );
		} ?>
	</h1>
	<?php
	// Load subtitle template
	boldlab_core_template_part( 'title/layouts/standard', 'templates/parts/subtitle', '', boldlab_core_get_standard_title_layout_subtitle_text() ); ?>
</div>