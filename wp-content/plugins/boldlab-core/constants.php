<?php

define( 'BOLDLAB_CORE_VERSION', '1.0.3' );
define( 'BOLDLAB_CORE_ABS_PATH', dirname( __FILE__ ) );
define( 'BOLDLAB_CORE_REL_PATH', dirname( plugin_basename( __FILE__ ) ) );
define( 'BOLDLAB_CORE_URL_PATH', plugin_dir_url( __FILE__ ) );
define( 'BOLDLAB_CORE_ASSETS_PATH', BOLDLAB_CORE_ABS_PATH . '/assets' );
define( 'BOLDLAB_CORE_ASSETS_URL_PATH', BOLDLAB_CORE_URL_PATH . 'assets' );
define( 'BOLDLAB_CORE_INC_PATH', BOLDLAB_CORE_ABS_PATH . '/inc' );
define( 'BOLDLAB_CORE_INC_URL_PATH', BOLDLAB_CORE_URL_PATH . 'inc' );
define( 'BOLDLAB_CORE_CPT_PATH', BOLDLAB_CORE_INC_PATH . '/post-types' );
define( 'BOLDLAB_CORE_CPT_URL_PATH', BOLDLAB_CORE_INC_URL_PATH . '/post-types' );
define( 'BOLDLAB_CORE_SHORTCODES_PATH', BOLDLAB_CORE_INC_PATH . '/shortcodes' );
define( 'BOLDLAB_CORE_SHORTCODES_URL_PATH', BOLDLAB_CORE_INC_URL_PATH . '/shortcodes' );
define( 'BOLDLAB_CORE_HEADER_LAYOUTS_PATH', BOLDLAB_CORE_INC_PATH . '/header/layouts' );
define( 'BOLDLAB_CORE_HEADER_LAYOUTS_URL_PATH', BOLDLAB_CORE_INC_URL_PATH . '/header/layouts' );
define( 'BOLDLAB_CORE_HEADER_ASSETS_PATH', BOLDLAB_CORE_INC_PATH . '/header/assets' );
define( 'BOLDLAB_CORE_HEADER_ASSETS_URL_PATH', BOLDLAB_CORE_INC_URL_PATH . '/header/assets' );

define( 'BOLDLAB_CORE_MENU_NAME', 'boldlab_core_menu' );
define( 'BOLDLAB_CORE_OPTIONS_NAME', 'boldlab_core_options' );

define( 'BOLDLAB_CORE_PROFILE_SLUG', 'edge' );