<?php
/*
Plugin Name: Boldlab Core
Plugin URI: https://qodeinteractive.com
Description: Plugin that adds portfolio post type, shortcodes and other modules
Author: Edge Themes
Author URI: https://qodeinteractive.com
Version: 1.0.3
*/

if ( ! class_exists( 'BoldlabCore' ) ) {
	class BoldlabCore {
		private static $instance;

		function __construct() {
			$this->require_core();

			add_filter( 'qode_framework_filter_register_admin_options', array( $this, 'create_core_options' ) );

			add_action( 'qode_framework_action_before_options_init_' . BOLDLAB_CORE_OPTIONS_NAME, array(
				$this,
				'init_core_options'
			) );

			add_action( 'qode_framework_action_populate_meta_box', array( $this, 'init_core_meta_boxes' ) );

			// Include plugin assets
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ) );

			// Make plugin available for translation
			add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ) );

			// Add plugin's body classes
			add_filter( 'body_class', array( $this, 'add_body_classes' ) );

			// Hook to include additional modules when plugin loaded
			do_action( 'boldlab_core_action_plugin_loaded' );
		}

		public static function get_instance() {
			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;
		}

		function require_core() {
			require_once 'constants.php';
			require_once BOLDLAB_CORE_ABS_PATH . '/helpers/helper.php';

			// Hook to include additional files before modules inclusion
			do_action( 'boldlab_core_action_before_include_modules' );

			foreach ( glob( BOLDLAB_CORE_INC_PATH . '/*/include.php' ) as $module ) {
				include_once $module;
			}

			// Hook to include additional files after modules inclusion
			do_action( 'boldlab_core_action_after_include_modules' );
		}

		function create_core_options( $options ) {
			$boldlab_core_options_admin = new QodeFrameworkOptionsAdmin(
				BOLDLAB_CORE_MENU_NAME,
				BOLDLAB_CORE_OPTIONS_NAME,
				array(
					'label' => esc_html__( 'Boldlab Core Options', 'boldlab-core' )
				)
			);
			$options[]                  = $boldlab_core_options_admin;

			return $options;
		}

		function init_core_options() {
			$qode_framework = qode_framework_get_framework_root();

			if ( ! empty( $qode_framework ) ) {
				$page = $qode_framework->add_options_page(
					array(
						'scope'       => BOLDLAB_CORE_OPTIONS_NAME,
						'type'        => 'admin',
						'slug'        => '',
						'title'       => esc_html__( 'General', 'boldlab-core' ),
						'description' => esc_html__( 'General Core Options', 'boldlab-core' ),
						'icon'        => 'fa fa-cog'
					)
				);

				// Hook to include additional options after default options
				do_action( 'boldlab_core_action_default_options_init', $page );
			}
		}

		function init_core_meta_boxes() {
			do_action( 'boldlab_core_action_default_meta_boxes_init' );
		}

		function enqueue_assets() {
			// CSS and JS dependency variables
			$style_dependency_array  = apply_filters( 'boldlab_core_filter_style_dependencies', array( 'boldlab-style-handle-main' ) );
			$script_dependency_array = apply_filters( 'boldlab_core_filter_script_dependencies', array( 'boldlab-script-handle-main-js' ) );

			// Hook to include additional scripts before plugin's main style
			do_action( 'boldlab_core_action_before_main_css' );

			// Enqueue plugin's main style
			wp_enqueue_style( 'boldlab-core-style', BOLDLAB_CORE_URL_PATH . 'assets/css/boldlab-core.min.css', $style_dependency_array );

			// Enqueue plugin's 3rd party scripts
			wp_enqueue_script( 'jquery-ui-core' );
			wp_enqueue_script( 'modernizr', BOLDLAB_CORE_URL_PATH . 'assets/plugins/modernizr/modernizr.js', array( 'jquery' ), false, true );
			wp_enqueue_script( 'tweenmax', BOLDLAB_CORE_URL_PATH . 'assets/plugins/tweenmax/tweenmax.min.js', '', false, true );
			wp_enqueue_script( 'scroll-to', BOLDLAB_CORE_URL_PATH . 'assets/plugins/scroll-to/ScrollToPlugin.min.js', '', false, true );

			// Hook to include additional scripts before plugin's main script
			do_action( 'boldlab_core_action_before_main_js' );

			// Enqueue plugin's main script
			wp_enqueue_script( 'boldlab-core-script', BOLDLAB_CORE_URL_PATH . 'assets/js/boldlab-core.min.js', $script_dependency_array, false, true );
		}

		function load_plugin_textdomain() {
			load_plugin_textdomain( 'boldlab-core', false, BOLDLAB_CORE_REL_PATH . '/languages' );
		}

		function add_body_classes( $classes ) {
			$classes[] = 'boldlab-core-' . BOLDLAB_CORE_VERSION;

			return $classes;
		}
	}
}

if ( ! function_exists( 'boldlab_core_instantiate_plugin' ) ) {
	function boldlab_core_instantiate_plugin() {
		BoldlabCore::get_instance();
	}

	add_action( 'qode_framework_action_load_dependent_plugins', 'boldlab_core_instantiate_plugin' );
}

if ( ! function_exists( 'boldlab_core_activation_trigger' ) ) {
	function boldlab_core_activation_trigger() {
		// Hook to add additional code on plugin activation
		do_action( 'boldlab_core_action_on_activation' );
	}

	register_activation_hook( __FILE__, 'boldlab_core_activation_trigger' );
}
if ( ! function_exists( 'boldlab_core_deactivation_trigger' ) ) {
	function boldlab_core_deactivation_trigger() {
		// Hook to add additional code on plugin deactivation
		do_action( 'boldlab_core_action_on_deactivation' );
	}

	register_deactivation_hook( __FILE__, 'boldlab_core_deactivation_trigger' );
}


if ( ! function_exists( 'boldlab_core_check_requirements' ) ) {
	function boldlab_core_check_requirements() {
		if ( ! defined( 'QODE_FRAMEWORK_VERSION' ) ) {
			add_action( 'admin_notices', 'boldlab_core_admin_notice_content' );
		}
	}

	add_action( 'plugins_loaded', 'boldlab_core_check_requirements' );
}

if ( ! function_exists( 'boldlab_core_admin_notice_content' ) ) {
	function boldlab_core_admin_notice_content() {
		echo sprintf( '<div class="notice notice-error"><p>%s</p></div>', esc_html__( 'Boldlab Framework plugin is required for Boldlab Core plugin to work properly. Please install/activate it first.', 'boldlab-core' ) );

		if ( function_exists( 'deactivate_plugins' ) ) {
			deactivate_plugins( plugin_basename( __FILE__ ) );
		}
	}
}