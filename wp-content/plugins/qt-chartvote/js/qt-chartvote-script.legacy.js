/**
 * @package QT Chartvote
 * Script for the Qantumthemes Chart Vote
 * V. 1.2
 */


(function($) {
	"use strict";
	$.fn.qtChartvoteInit = function(){

		$("body a.qt-chartvote-link").each(function(i,c){
			var t = $(c);
			var cookiename = 'voted-'+t.data('chartid')+'-'+t.data('position');
			if(null !== $.cookie(cookiename) ) {
				t.on("click",function(e){
					e.stopPropagation();
					e.preventDefault();
					return;
				});
				t.parent().css({'cursor': 'not-allowed'}).addClass('vote-disabled').find('a').css({'color':'#ddd'});
			}
		});
		

		$("body a.qt-chartvote-link").on("click",function(e){
			e.stopPropagation();
			e.preventDefault();
			var t = $(this);
			if( t.parent().hasClass('vote-disabled') ){
				return;
			}

			var cookiename = 'voted-'+t.data('chartid')+'-'+t.data('position');
			t.parent().css({'cursor': 'not-allowed'}).addClass('vote-disabled').find('a').css({ 'color':'#ddd'});

			if(null == $.cookie(cookiename) ) {
				$.ajax({
					type: "post",
					url: chartvote_ajax_var.url,
					cache: false,
					data: "action=track-vote&nonce="+chartvote_ajax_var.nonce+"&position="+t.data('position')+"&move="+t.data('move')+"&chartid="+t.data('chartid'),
					success: function(data){
						if( '1' !== $.cookie(cookiename)){
							var dataarr = jQuery.parseJSON(data);
							t.parent().find(".qt-chartvote-number").html(dataarr.newvalue).css({'font-weight':'bold'});
							$.cookie(cookiename, '1', { path: '/' }); // session cookie only
						} else {
							alert("You already voted for this track");
						}
						return;
					},
					error: function(e){
						alert("Sorry, we can't contact the server in this moment. Please try later.");
						return;
					}
				});
			} else {
				alert("Sorry, you already voted this track.");
				return;
			}
			return;
		});
	};
})(jQuery);