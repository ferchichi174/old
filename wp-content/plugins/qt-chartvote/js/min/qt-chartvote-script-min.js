/**
 * @package QT Chartvote
 * Script for the Qantumthemes Chart Vote
 * V. 1.2
 */
!function(r){"use strict";r.fn.qtChartvoteInit=function(){r("body a.qt-chartvote-link").off("click"),r("body a.qt-chartvote-link").each(function(a,t){var e=r(t),o="voted-"+e.data("chartid")+"-"+e.data("position");"1"==r.cookie(o)&&(r(t).addClass("disabled"),r(t).parent().addClass("disabled"))
/*if(null !== $.cookie(cookiename) ) {
				t.on("click",function(e){
					e.stopPropagation();
					e.preventDefault();
					return;
				});
				t.parent().css({'cursor': 'not-allowed'}).addClass('vote-disabled').find('a').css({'color':'#ddd'});
			}*/}),r("body a.qt-chartvote-link").on("click",function(a){a.preventDefault(),a.stopPropagation();var e=r(this),t="voted-"+e.data("chartid")+"-"+e.data("position");"1"==r.cookie(t)?e.addClass("disabled"):(r.cookie(t,"1",{expires:1,path:"/"}),r.ajax({type:"post",url:chartvote_ajax_var.url,cache:!1,data:"action=track-vote&nonce="+chartvote_ajax_var.nonce+"&position="+e.data("position")+"&move="+e.data("move")+"&chartid="+e.data("chartid"),success:function(a){var t=jQuery.parseJSON(a);e.parent().find(".qt-chartvote-number").html(t.newvalue),e.parent().find("a").addClass("disabled"),e.parent().addClass("disabled")},error:function(a){alert("Sorry, we can't contact the server at the moment, please try later."),console.log(a.Error)}}))})}}(jQuery);