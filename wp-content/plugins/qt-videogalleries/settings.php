<?php
/*
 Part of QT Subpages
*/


include 'inc/QT_var_names.php';
include 'inc/QT_create_form.php'; 

if(!function_exists('qtHexToRGBA')){
function qtHexToRGBA($hex){
	$hex = str_replace('#', '', $hex);
    if (strlen($hex) == 3) {
        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
    }
    $r = hexdec(substr($hex,0,2));
    $g = hexdec(substr($hex,2,2));
    $b = hexdec(substr($hex,4,2));
    return $r.','.$g.','.$b;
}
}
add_action('admin_menu', 'vdl_create_optionspage');


/* = Create options page
=============================================================*/


if(!function_exists('vdl_create_optionspage')){
function vdl_create_optionspage() {
	add_options_page('VideoLove', 'VideoLove', 'manage_options', 'vdl_settings', 'vdl_options');
}
}
if(!function_exists('vdl_options')){
	function vdl_options() {
		if (!current_user_can('manage_options'))  {
			wp_die( __('You do not have sufficient permissions to access this page.') );
		}
	?>
	<h2 class="VideoLoveTitle">VideoLove: Setup your video galleries here</h2>
	<div class=wrap>
	    <form method="post" id="VideoloveSettingsForm" class="qantumthemes_form" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
	        <h2>Options</h2>
	        <?php 
				echo VDL_FORM;
			?>
			<p class="submit">
			<input type="submit" name="submit" value="<?php _e('Update options &raquo;'); ?>"  class="button button-primary button-large" />
			</p>
	    </form>
	    <div class="qtspanel">
	    	<h2>Instructions</h2>
	    	<p>Place the shortcode in the page that will contain the gallery.</p>
	    	<pre>[videolove showfilters="yes" showtitle="yes" showtags="yes" defaultimg="" parent="" columns="4"]</pre>

		    <h2>Parameters</h2>
		    <table class="qtsparameters">
		    	<tr><td><strong>showfilters:</strong></td><td>[optional] If "yes", the filters will appear above the subpages list</td></tr>
		    	<tr><td><strong>showtitle:</strong></td><td>[optional] If "yes", the title will appear over each item when mouse over</td></tr>
		    	<tr><td><strong>showtags:</strong></td><td>[optional] If "yes", the tags will appear over each item when mouse over</td></tr>
		    	<tr><td><strong>columns:</strong></td><td>[optional] Can be 2, 3 or 4. Simply specify the number. Default is 4 columns</td></tr>
		    	<tr><td><strong>quantity:</strong></td><td>[optional] Amount of videos, default is -1 (all)</td></tr>

		    	<tr><td><strong>defaultimg:</strong></td><td>[optional] Default image in case no featured image is specified for that child page<br>Default: <?php echo plugins_url( 'assets/default.jpg' , __FILE__ ); ?></td></tr>
		    	<tr><td><strong>showpreview:</strong></td><td>[optional] If "yes", a popup will show the preview</td></tr>
		    </table>
	    </div>
	    <div class="qtspanel signpanel">
	    	<a href="http://www.qantumthemes.com/" target="_blank">
			<img src="<?php echo plugins_url( 'assets/qantum-logo-web.png' , __FILE__ ); ?>" />
			</a>
			<div class="canc"></div>
	    </div>
	</div>
	<?php 
	}
}


/* = Custom css added in html head
=============================================================*/

if(!function_exists('vdl_customstylesx')){
function vdl_customstylesx(){
		$vdl_overlay_color = get_option('vdl_overlay_color');
		$alpha = get_option('vdl_overlay_opacity');
		$titlecolor = get_option('vdl_title_color');
		$vdl_tags_color = get_option('vdl_tags_color');
		$vdl_filters_color = get_option('vdl_filters_color');
		$vdl_filters_bordercolor = get_option('vdl_filters_bordercolor');
		$vdl_filters_bordershape = get_option('vdl_filters_bordershape');
		$vdl_filters_fontsize = get_option('vdl_filters_fontsize');
		$vdl_title_size_mobile = get_option('vdl_title_size_mobile');
		$vdl_filters_bgcolor = get_option('vdl_filters_bgcolor');
		$vdl_filters_bgcolorh = get_option('vdl_filters_bgcolorh');
		$vdl_preview_background = get_option('vdl_preview_background');
		$vdl_preview_text_color = get_option('vdl_preview_text_color');
		$vdl_preview_buttons_color = get_option('vdl_preview_buttons_color');
		$vdl_preview_buttons_color_h = get_option('vdl_preview_buttons_color_h');
			
		echo '
		<!-- CSS styles added by QT Subpages Plugin -->
		<style type="text/css">
			'.(($vdl_overlay_color!='')? '.vdl-subpages-item  a .detail {background: rgba('.qtHexToRGBA(get_option('vdl_overlay_color')).','.(($alpha != '')? (get_option('vdl_overlay_opacity')/100) : ".8").') !important;} ':'').'
			.vdl-subpages-item  a .detail .title {font-size:'.get_option( 'vdl_title_size', "13" ).'px; '.(($titlecolor!='')? ' color:'.$titlecolor.' !important;':'').'}
			'.(($vdl_tags_color != '')? ".vdl-subpages-item  a .detail p.trmlist span.trm {border-color:".$vdl_tags_color ." !important;color:".$vdl_tags_color ."!important;}" : "").'
			.vdl-subpages-item  a .detail {padding-top:'.get_option( 'vdl_hover_top', "45" ).'px !important;}
			.vdl-subpages-container ul.vdl-subpages-tagcloud li a {
				'.(($vdl_filters_fontsize != '')? "font-size:".$vdl_filters_fontsize."px;" : "").'
				'.(($vdl_filters_color != '')? "color:".$vdl_filters_color .";" : "").'
				'.(($vdl_filters_bgcolor != '')? "background-color:".$vdl_filters_bgcolor .";" : "").'
				'.(($vdl_filters_bordercolor != '')? "border-color:".$vdl_filters_bordercolor ."; " : "").'
				'.(($vdl_filters_bordershape != '')? "border-radius:".$vdl_filters_bordershape ."px;" : "").'
			}
			.vdl-subpages-container ul.vdl-subpages-tagcloud li a:hover {'.(($vdl_filters_bgcolorh != '')? "background-color:".$vdl_filters_bgcolorh .";" : "").'}
			'.(($vdl_preview_background != '')? ".vdl-preview-container.open .vdl-preview-content {background-color: ".$vdl_preview_background.";}" : "").'
			'.(($vdl_preview_text_color != '')? ".vdl-preview-container.open .vdl-preview-content {color: ".$vdl_preview_text_color.";}" : "").'
			'.(($vdl_preview_buttons_color != '')? ".vdl-preview-container.open .vdl-preview-prev,.vdl-preview-container.open .vdl-preview-next,.vdl-preview-container .vdl-preview-content a.vdl-project-link, .vdl-preview-content::-webkit-scrollbar-thumb {background-color: ".$vdl_preview_buttons_color." !important;}
			  img.vdl-imgpreview {border-color:".$vdl_preview_buttons_color." !important;} " : "").'
			'.(($vdl_preview_buttons_color_h != '')? ".vdl-preview-container.open a.vdl-preview-prev:hover, .vdl-preview-container.open a.vdl-preview-next:hover, .vdl-preview-container .vdl-preview-content a.vdl-project-link:hover {background-color: ".$vdl_preview_buttons_color_h." !important;}" : "").'
			@media (max-width: 768px){
				.vdl-elementcontents a.vdl-link .detail .title {'.(($vdl_title_size_mobile != '')? "font-size:".$vdl_title_size_mobile."px;" : "").'}
			}
			'.get_option('vdl_customcss').'
		</style>
		';	
	}
}
add_action('wp_head','vdl_customstylesx',99999999);
?>