/*
	Script used by Submatic admin screen
*/

jQuery.noConflict();
(function($){
	$("#VideoloveSettingsForm a.submatic-tabtitle").click(function(e){
		e.preventDefault();
		var sid = $(this).attr("data-section");
		$("#VideoloveSettingsForm .submatic-section").removeClass("open");
		$("#VideoloveSettingsForm #"+sid).addClass("open");
	});
})(jQuery)