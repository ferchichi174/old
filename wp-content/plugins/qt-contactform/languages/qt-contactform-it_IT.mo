��          �            h  3   i  9   �     �     �  
   �            	   $     .     6  <   C     �  -   �     �  �  �  F   �  G   �     -     I     O     T     c     x  
   �     �  F   �     �  (   �          
                                                         	                  Attention: Invalid recipient in the page's settings Attention: recipient email missing in the page's settings Contact from your website  Email First name Go Back I've read and accept the Last name Message Message sent Some fields are missing, please check the data and try again Submit Thank you, we will answer as soon as possible privacy terms Project-Id-Version: QT ContactForm plugin
POT-Creation-Date: 2017-08-10 22:47+0200
PO-Revision-Date: 
Language-Team: QantumThemes
Report-Msgid-Bugs-To: Translator Name <translations@example.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Textdomain-Support: yesX-Generator: Poedit 1.6.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html_x:1,2c;esc_html__;esc_attr_e;esc_attr_x:1,2c;esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_x:1,2c;_n:1,2;_n_noop:1,2;__ngettext:1,2;__ngettext_noop:1,2;_c,_nc:4c,1,2
X-Poedit-Basepath: ..
X-Generator: Poedit 1.8.9
Last-Translator: 
Language: it
X-Poedit-SearchPath-0: .
 Attenzione: Il destinatario non valido nelle impostazioni della pagina Attenzione: destinatario email mancanti nelle impostazioni della pagina Contattare dal tuo sito Web Email Nome Torna Indietro Ho letto e accetto i Cognome Messaggio	 Messaggio inviato Alcuni campi sono mancanti, si prega di controllare i dati e riprovare Invia messaggio Grazie, vi risponderemo appena possibile Termini della privacy 