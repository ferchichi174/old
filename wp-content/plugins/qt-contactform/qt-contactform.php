<?php
/*
Plugin Name: QT Contactform
Plugin URI: http://qantumthemes.com
Description: Adds contact form and details to a page
Version: 1.1
Author: QantumThemes
Author URI: http://qantumthemes.com
Text Domain: qt-contactform
Domain Path: /languages
*/


/**
 * 	language files
 * 	=============================================
 */
if(!function_exists('qt_contactform_load_text_domain')){
function qt_contactform_load_text_domain() {
	load_plugin_textdomain( 'qt-contactform', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}}
add_action( 'init', 'qt_contactform_load_text_domain' );


if (!function_exists('qt_contactform')){
	function qt_contactform(){
		ob_start();

		global $post;
		$submission_nonce = wp_create_nonce( 'contactform_sending_secret-' . $post->ID );
		$recipient = get_post_meta($post->ID, "qt_contacts_recipient", true);
		$subject =  get_post_meta($post->ID, "qt_contacts_subject", true);
		if( $subject == '') {
			$subject = esc_attr__('Contact from your website ', "qt-contactform").get_site_url();
		}


		/**
		 * Submission autenticity test for safety against spam attacks
		 */
		if(isset($_POST) && isset($_GET) && $recipient != ''){
			if(array_key_exists("_qtform_snonce", $_POST) && array_key_exists('qtSendEmail', $_GET)) {
				/**
				 * 	Antispam check
				 * 	===============================
				 */
				if(array_key_exists('url', $_POST)) {
					if($_POST['url'] !== '') {
						die( 'Security check failed. Spam attempt detected' ); 
					}
				}
				$nonce = $_POST['_qtform_snonce'];
				if ( ! wp_verify_nonce( $nonce, 'contactform_sending_secret-'. $post->ID ) ) {
					// This nonce is not valid.
					die( 'Security check failed. Spam attempt detected' ); 
				} else {
					/*======================================================
					
					DYNAMIC EMAIL SENDING CODE

					======================================================*/
					$fields = array('first_name','last_name','email','message'); // LIST OF THE FIELDS

					// $qt_contacts_privacy = get_post_meta($post->ID, 'qt_contacts_privacy', true);
					//         	if($qt_contacts_privacy != ''){
					//         		$fields[] = 'privacy';
					//         	}


					$errors = ''; // ERROR MESSAGE
					// FOR EVERY FIELD WE CHECK IF IT'S COMPILED, OTHERWISE DISPLAY THE ERROR
					foreach($fields as $fieldname){
						if(!array_key_exists($fieldname, $_POST)) { // FIELD NOT EXISTING
							$errors .= $fieldname.'[#]';
						} else {
							if($_POST[$fieldname] == ''){ // OR FIELD EMPTY
								$errors .= $fieldname.'[#]';
							}
						}
					}
					if($errors != '') { // IF WE HAVE ERRORS, DISPLAY THE ERROR MESSAGE
						?>
						<div class="thankyou">
							<h2 class="qt-section-title">ERROR<i class="deco"></i></h2>
							<h4 class="qt-spacer-s"><?php echo esc_attr__("Some fields are missing, please check the data and try again","qt-contactform"); ?></h4>
							<p class="qt-spacer-s"><a href="#" onclick="goBack()" class="qt-btn qt-btn-secondary"><?php  echo esc_attr__("Go Back","qt-contactform"); ?></a></p>
							<hr class="qt-spacer-m">
						</div>
						<?php  
					} else { // IF ALL FIELDS ARE COMPILED LET'S GO ON
						if(isset($_POST['privacy'])){
							$privacy = $_POST['privacy'];
						}
						$first_name = $_POST['first_name'];
						$last_name = $_POST['last_name'];
						$email = $_POST['email'];
						$message = str_replace("\n.", "\n..",$_POST['message']);

						$headers = 'From: '.$first_name.' '.$last_name.' <' . $recipient .'>'."\r\n" .
							'Reply-To:  '.$first_name.' '.$last_name.' <' . $email .'>'."\r\n" .
							'X-Mailer: PHP/' . phpversion();
						if(wp_mail($recipient, $subject, $message, $headers)){
							?>
							<div class="thankyou">
								<h2 class="qt-section-title"><?php  echo esc_attr__("Message sent","qt-contactform"); ?><i class="deco"></i></h2>
								<h4 class="qt-spacer-s"><?php  echo esc_attr__("Thank you, we will answer as soon as possible","qt-contactform"); ?></h4>
								<hr class="qt-spacer-m">
							</div>
							<?php  
						} else {
							?>
							<div class="thankyou">
								<h2 class="qt-section-title"><?php  echo esc_attr__("Error","qt-contactform"); ?><i class="deco"></i></h2>
								<h4 class="qt-spacer-s"><?php  echo esc_attr__("Sorry, because of a technical error is not possible to delivery your message. Please write us manually at our email address.","qt-contactform"); ?></h4>
								<hr class="qt-spacer-m">
							</div>
							<?php  

						}
					}
				}
			}
		}

		?>
		<!-- ====================== SECTION BOOKING AND CONTACTS ================================================ -->
		<div id="booking" class="section qt-section-booking qt-card" data-bottom-top="opacity:0;"  data-center-top="opacity:1;">
			<div class="qt-valign-wrapper">
				<div class="qt-valign flow-text">
					<div class="qt-booking-form" >
						<ul class="tabs">
							<li class="tab col s4"><h5><a href="#form" class="active"><?php echo esc_attr(get_post_meta( get_the_id(), 'qt_contacts_form_title', true )); ?></a></h5></li>
							<li class="tab col s4"><h5><a href="#contacts"><?php echo esc_attr(get_post_meta( get_the_id(), 'qt_contacts_data_title', true )); ?></a></h5></li>
							<?php if(get_post_meta( get_the_id(), 'qt_contacts_map', true ) !== ''){ ?>
								<li class="tab col s4"><h5><a href="#map"><?php echo esc_attr(get_post_meta( get_the_id(), 'qt_contacts_map_title', true )); ?></a></h5></li>
							<?php } ?>
						</ul>
						<div id="form" class="row">
							<form class="col s12" method="post" action="<?php echo add_query_arg("qtSendEmail","1", get_the_permalink()); ?>">
								<h3 class="left-align qt-vertical-padding-m"><?php echo esc_attr(get_post_meta( get_the_id(), 'qt_contacts_form_title', true )); ?></h3>
								<?php  
								if($recipient === ''){
									echo esc_attr__("Attention: recipient email missing in the page's settings", "qt-contactform");
								} else {
									if (!filter_var($recipient, FILTER_VALIDATE_EMAIL)) {
										echo esc_attr__("Attention: Invalid recipient in the page's settings", "qt-contactform");
									} else {
										?>
										<div class="row">
											<div class="input-field col s6">
											  <input name="first_name" id="first_name" type="text" class="validate">
											  <label for="first_name"><?php echo esc_attr__("First name", "qt-contactform"); ?></label>
											</div>
											<div class="input-field col s6">
											  <input name="last_name" id="last_name" type="text" class="validate">
											  <label for="last_name"><?php echo esc_attr__("Last name", "qt-contactform"); ?></label>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s12">
											  <input name="email" id="formemail" type="email" class="validate">
											  <label for="email"><?php echo esc_attr__("Email", "qt-contactform"); ?></label>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s12">
												<textarea name="message" id="message" class="materialize-textarea" maxlength="300"></textarea>
												<label for="message"><?php echo esc_attr__("Message", "qt-contactform"); ?></label>
											</div>
										</div>
										<?php  
										$qt_contacts_privacy = get_post_meta($post->ID, 'qt_contacts_privacy', true);
										if($qt_contacts_privacy != ''):
										?>
										<div class="row">
											<div class="input-field col s12">
												<input name="privacy" type="checkbox" id="privacy" value="1" />
												<label for="privacy"><?php echo esc_attr__("I've read and accept the", "qt-contactform"); ?> <a href="<?php echo esc_url($qt_contacts_privacy) ?>" target="_blank"><?php echo esc_attr__("privacy terms", "qt-contactform"); ?></a>.</label>
											</div>
										</div>
										<?php endif; ?>

										<div class="row">
											<div class="input-field col s12">
												<button class="qt-btn qt-btn-l qt-btn-primary qt-spacer-m waves-effect waves-light" type="submit" name="action">
													<span class="lnr lnr-rocket"></span> <?php  echo esc_attr__("Submit", "qt-contactform"); ?>
												</button>
											</div>
										</div>

										<?php  
									}
								}
								?>

								<?php  
								/**
								 * 	Anti-XSS protection with WP nonce:
								 * 	=======================================================
								 */
								?>
								<input type="hidden" name="_qtform_snonce" value="<?php echo esc_attr($submission_nonce); ?>">
								<?php  
								/**
								 * 	Hidden antispam field:
								 * 	=======================================================
								 */
								?>
								<p class="qt-antispam"><input type="text" name="url" /></p>

							</form>
						</div>
						<div id="contacts" class="row qt-contacts">
							<div class="col s12">
								<h3 class="left-align qt-vertical-padding-m"><?php echo esc_attr(get_post_meta( get_the_id(), 'qt_contacts_data_title', true )); ?></h3>
								<?php if(get_post_meta( get_the_id(), 'qt_contacts_phone', true ) !== ''){ ?>
									<p><i class="qt-bigicon dripicons-phone"></i><span><?php echo esc_attr(get_post_meta( get_the_id(), 'qt_contacts_phone', true )); ?></span></p>
								<?php } ?>
								<?php if(get_post_meta( get_the_id(), 'qt_contacts_email', true ) !== ''){ ?>
									<p><i class="qt-bigicon dripicons-mail"></i><span><?php echo esc_attr(get_post_meta( get_the_id(), 'qt_contacts_email', true )); ?></span></p>
								<?php } ?>
								<?php if(get_post_meta( get_the_id(), 'qt_contacts_address', true ) !== ''){ ?>
									<p><i class="qt-bigicon dripicons-location"></i><span><?php echo esc_attr(get_post_meta( get_the_id(), 'qt_contacts_address', true )).'<br>'.esc_attr(get_post_meta( get_the_id(), 'qt_contacts_address2', true )); ?></span></p>
								<?php } ?>
							</div>
						</div>
						<?php if(get_post_meta( get_the_id(), 'qt_contacts_map', true ) !== ''){ ?>
						<div id="map" class="row qt-map-nomargin">
							<div class="col s12">
								<h3 class="left-align qt-vertical-padding-m"><?php echo esc_attr(get_post_meta( get_the_id(), 'qt_contacts_map_title', true )); ?></h3>
								<<?php echo esc_attr("iframe ") ?> src="<?php echo esc_attr(get_post_meta( get_the_id(), 'qt_contacts_map', true )); ?>" width="600" height="450" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<!-- ====================== SECTION BOOKING AND CONTACTS END ================================================ -->
		<?php  
		return ob_get_clean();
	}
} // function end

add_shortcode("qt-contactform", "qt_contactform");
