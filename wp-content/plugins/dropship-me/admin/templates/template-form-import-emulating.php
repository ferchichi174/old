<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$tmpl = new \dm\dmTemplate();
?>
<span class="mobile_detach">
	<div class="row box-shadow mt-3 py-4 import-settings no-gutters">
		<div class="col-12 col-md-6 col-lg-3">
			<?php
			echo $tmpl->switcher( [
					'name'  => '',
					'id'  => 'create_cat_e',
					'value' => 1,
					'label' => __( 'Create categories from Dropship Me', 'dm' )
				] );
			?>
		</div>
		<div class="col-12 col-md-6 col-lg-3">
			<?php
			echo $tmpl->switcher( [
					'name'  => '',
					'id'  => 'attributes_e',
					'value' => 1,
					'label' => __( 'Remove item specifics', 'dm' )
				] );
			?>
		</div>
		<div class="col-12 col-md-6 col-lg-3">
			<?php
			echo $tmpl->switcher( [
					'name'  => '',
					'id'  => 'publish_e',
					'value' => 1,
					'label' => __( 'Publish products', 'dm' )
				] );
			?>
		</div>
		<div class="col col-md-6 col-lg-3">
			<?php
			echo $tmpl->switcher( [
					'name'  => '',
					'id'  => 'recommended_price_e',
					'value' => 1,
					'label' => __( 'Import with recommended prices', 'dm' )
				] )
			?>
		</div>
	</div>
</span>
