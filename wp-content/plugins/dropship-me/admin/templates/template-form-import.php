<?php
/**
 * Author: Vitaly Kukin
 * Date: 19.09.2018
 * Time: 8:47
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$tmpl = new \dm\dmTemplate();
?>
<span class="">
	<div class="row box-shadow mt-3 py-4 import-settings no-gutters">
		<div class="col-12 col-md-6 col-lg-3">
			<?php
			echo $tmpl->switcher( [
					'name'  => 'create_cat',
					'value' => 1,
					'label' => __( 'Create categories from Dropship Me', 'dm' )
				] );
			?>
		</div>
		<div class="col-12 col-md-6 col-lg-3">
			<?php
			echo $tmpl->switcher( [
					'name'  => 'attributes',
					'value' => 1,
					'label' => __( 'Remove item specifics', 'dm' )
				] );
			?>
		</div>
		<div class="col-12 col-md-6 col-lg-3">
			<?php
			echo $tmpl->switcher( [
					'name'  => 'publish',
					'value' => 1,
					'label' => __( 'Publish products', 'dm' )
				] );
			?>
		</div>
		<div class="col col-md-6 col-lg-3">
			<?php
			echo $tmpl->switcher( [
					'name'  => 'recommended_price',
					'value' => 1,
					'label' => __( 'Import with recommended prices', 'dm' )
				] )
			?>
		</div>
	</div>
</span>
