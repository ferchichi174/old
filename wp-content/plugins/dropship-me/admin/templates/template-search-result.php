<?php
/**
 * Author: Vitaly Kukin
 * Date: 14.09.2018
 * Time: 14:52
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$tmpl = new \dm\dmTemplate();
?>

{{#each products}}
<!-- item result begin-->
<div class="product-item-list box-shadow position-relative" data-id="{{id}}" data-already="{{already}}">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
    <div class="check-box">
        <?php
        echo $tmpl->checkbox( [
	        'value' => '{{@index}}',
	        'id'    => 'check-item-{{@index}}'
        ] )
        ?>
    </div>
    <div class="row no-gutters align-items-center" id="product-{{id}}">
        <div class="col">
            <div class="d-flex align-items-center">
                <div class="product-image">
                    {{#if imageUrl}}
                    <a href="{{imageFull}}" data-fancybox="thumb-{{@index}}">
                        <img src="{{imageUrl}}" class="img-fluid">
                    </a>
                    {{/if}}
                </div>
                <div class="product-title">
                    <h3>{{productTitle}}</h3>
                    <div class="rate-block-small d-none d-sm-block">
                        <div class="stars-box d-inline-block">
                            <div class="stars">
                                <div class="stars-line" style="width:{{ratePercent}}%"></div>
                            </div>
                        </div>
                        ({{productRate}}) <span class="separate">|</span> <?php _e( 'Orders', 'dm' ) ?>: {{purchaseVolume}}
                        {{#if free}}
                             <span class="separate">|</span> <i class="fa fa-plane color-green"></i> <?php _e( 'Free shipping', 'dm' ) ?>
                        {{/if}}
					</div>
                    {{#if date_import}}<div class="pt-1 small"><?php _e( 'Imported' ) ?>: {{date_import}}</div>{{/if}}
                </div>
            </div>
            <div class="d-block d-sm-none">
                 <div class="stars-box d-inline-block mt-3">
                     <div class="stars">
                         <div class="stars-line" style="width:{{ratePercent}}%"></div>
                     </div>
                 </div>
                 ({{productRate}}) <span class="separate">|</span> <?php _e( 'Orders', 'dm' ) ?>: {{purchaseVolume}}
                        {{#if free}}
                             <span class="separate">|</span> <i class="fa fa-plane color-green"></i> <?php _e( 'Free shipping', 'dm' ) ?>
                        {{/if}}
            </div>
            <div class="d-flex d-lg-none flex-column flex-sm-row align-items-left align-items-sm-center justify-content-around product-mobile-friendly pt-3">
                <div class="supplier-price">
                    <h5><?php _e( 'Supplier price', 'dm' ) ?>:</h5>
                    <h4 class="color-orange">{{format_price origPrices.origSalePrice}}</h4>
                </div>
                <div class="recommended-price d-block">
                    <h5><?php _e( 'Recommended', 'dm' ) ?>:</h5>
                    <h4>{{format_price prices.salePrice}}</h4>
                </div>
                <div class="profit-price d-block">
                    <h5><?php _e( 'Your profit', 'dm' ) ?>:</h5>
                    <h4>{{math_format prices.salePrice '-' origPrices.origSalePrice}}</h4>
                </div>
            </div>
        </div>
        <div class="col-xl-2 text-center rate-block-large">
            <div class="star-rate">
                <div class="stars-box d-inline-block">
                    <div class="stars">
                        <div class="stars-line" style="width:{{ratePercent}}%"></div>
                    </div>
                </div>
                ({{productRate}})
            </div>
            <div class="product-rate">
                <?php _e( 'Orders', 'dm' ) ?>: {{purchaseVolume}}
            </div>
            {{#if free}}
                <div class="has-free">
                    <i class="fa fa-plane color-green"></i> <?php _e( 'Free shipping', 'dm' ) ?>
                </div>
            {{/if}}
        </div>
        <div class="col-xl-5 col-lg-5 text-center d-none d-lg-flex flex-row align-items-center justify-content-around product-mobile-friendly">
            <div class="col-xl-4 col-lg-6">
                <div class="supplier-price">
                    <h5><?php _e( 'Supplier price', 'dm' ) ?>:</h5>
                    <h4 class="color-orange">{{format_price origPrices.origSalePrice}}</h4>
                </div>
            </div>
            <div class="col-xl-8 col-lg-6 price-block">
                <div class="recommended-price">
                    <h5><?php _e( 'Recommended', 'dm' ) ?>:</h5>
                    <h4>{{format_price prices.salePrice}}</h4>
                </div>
                <div class="profit-price">
                    <h5><?php _e( 'Your profit', 'dm' ) ?>:</h5>
                    <h4>{{math_format prices.salePrice '-' origPrices.origSalePrice}}</h4>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-2 col-lg-2 text-center text-md-right action-buttons pt-2 pt-md-0 d-none d-sm-block">
            <button class="btn btn-green ads-no first-btn js-import-product" {{#ifCond already "==" "1" }}disabled="disabled"{{/ifCond}}>
                {{#ifCond already "==" "1" }}
                    <?php _e( 'Imported', 'dm' ) ?>
                {{else}}
                    <i class="icon-plus-svg"></i>
                    {{#ifCond imported "==" "1" }}
                        <?php _e( 'Re-import', 'dm' ) ?>
                    {{else}}
                        <?php _e( 'Import', 'dm' ) ?>
                    {{/ifCond}}
                {{/ifCond}}
            </button>
            <button class="btn btn-green-transparent ads-no js-details" data-action="show">
                <?php _e( 'View Details', 'dm' ) ?>
            </button>
        </div>
		<div class="col-lg-3 d-block d-sm-none">
			<div class="text-center row action-buttons">
				<button class="col mr-2 ml-3 btn btn-green ads-no js-import-product" {{#ifCond already "==" "1" }}disabled="disabled"{{/ifCond}}>
					{{#ifCond already "==" "1" }}
						<?php _e( 'Imported', 'dm' ) ?>
					{{else}}
						<i class="icon-plus-svg"></i>
						{{#ifCond imported "==" "1" }}
							<?php _e( 'Re-import', 'dm' ) ?>
						{{else}}
							<?php _e( 'Import', 'dm' ) ?>
						{{/ifCond}}
					{{/ifCond}}
				</button>
				<button class="col ml-2 mr-3 btn btn-green-transparent ads-no js-details" data-action="show">
					<?php _e( 'View Details', 'dm' ) ?>
				</button>
			</div>
		</div>
    </div>
    <div class="product-info" id="supplier-{{id}}"></div>
</div>
<!-- item result end-->
{{/each}}
<div class="row py-3">
    <div class="col text-right">
        <div class="tab-nav-elements tab-nav-last d-inline-flex">
            <div class="pagination-menu jqpagination"></div>
        </div>
    </div>
</div>