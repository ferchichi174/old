=== DropshipMe ===

Contributors: dropshipme
Donate link: https://dropship.me/
Tags: ecommerce, wordpress, woocommerce, dropshipping, aliexpress, aliexpress dropshipping, alidropship, woocommerce aliexpress, import, import products, product import, import from aliexpress, woocommerce aliexpress importer, woocommerce aliexpress dropshipping, woocommerce product import, woocommerce import products, import products into woocommerce
Requires at least: 4.7    
Tested up to: 4.9
Stable tag: 0.9.2
Requires PHP: 5.6
License: MIT License/X11
License URI: http://www.opensource.org/licenses/mit-license.php

DropshipMe allows you to easily import selected AliExpress products with clear, already edited titles and descriptions to your WordPress store.

== Description ==

DropshipMe is a breakthrough product for AliExpress dropshipping. We curate a base of thousands of top selling and already 100% edited AliExpress products that integrates with WordPress-based stores. DropshipMe plugin allows you to easily search and import the best and promising products from the most reliable suppliers to your store. All products are handpicked by our team of experts and have already manually edited information, such as titles, descriptions, images, variants, etc. So you can have your perfectly pre-optimized product pages your customers expect to land on in absolutely no time. All the manual work is already done for you. Just import products you fancy in a couple of clicks and start selling.

DropshipMe plugin is free. The plugin provides the access to the product database via API. To activate the plugin, visit [DropshipMe website](https://dropship.me/plugin/), enter your email and click "Get my plugin now" to get your free API key. After the activation you immediately get 50 free products ready to import to your store. The plugin is compatible with AliDropship and WooCommerce.


https://www.youtube.com/watch?v=NCTOCW_Ar0s


**Features:**

- Quick and easy product search and import
- Only trustworthy AliExpress suppliers 
- Winning products handpicked by experts
- Professionally edited product information
- Import product ratings and reviews
- Regularly updated and growing database
- Recommended pricing markup

= Minimum Requirements =


* WordPress 4.7 or greater

* WooCommerce 3.0.0 or greater

* PHP version 5.6 or greater




= Support =


In case you have any questions or need technical assistance, get in touch with us in our online chat window [here](https://dropship.me/) or send us email at [support@dropship.me](mailto:support@dropship.me)

= Follow Us =


**Our Official Website** - [https://dropship.me/](https://dropship.me/)
**Our Facebook Page** - [https://www.facebook.com/dropshipme/](https://www.facebook.com/dropshipme/)
**Our Google+ Account** - [https://plus.google.com/100528644879101460627](https://plus.google.com/100528644879101460627)
**Our Twitter Account** - [https://twitter.com/dropship_me](https://twitter.com/dropship_me)


== Installation ==
To install the plugin, please follow this instruction.
1. Upload dropshipme.zip to /wp-admin/plugin-install.php
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Enter your API key gained from [https://dropship.me/](https://dropship.me/plugin/)
4. Start importing items and sell them in your WooCommerce store

== Frequently Asked Questions ==
= Is this tool free? =
Yes, the plugin is free. You will be also given 50 free product imports. Since the system requires a lot of memory and resources, if you want to have more than 50 product imports you'll need to purchase product packages. The plugin will keep running with free account.
= How to get DropshipMe API key? =
The plugin provides the access to DropshipMe Services via API. To activate the plugin, get your free API key on [DropshipMe website](https://dropship.me/plugin/).
= Where can I get support? =


To ask for help, report bugs or suggest changes, please email to [support@dropship.me](mailto:support@dropship.me).
= For More Query? =
You can get more Knowledge about DropshipMe from [Knowledgebase](https://help.dropship.me/).
== Changelog ==
= 0.9.6 =
Supplier price, orders amount and shipping details filters
Warehouse location info for products
Shipping details info for products
Check your import list section
Show prices in my currency
Product analysis info
Optimization
= 0.9.2 =
Woo import bug fix. Free shipping icon added
= 0.9.1 =
Minor optimizations
= 0.9.0 =
Minor optimizations
= 0.8.9.1 =
Minor optimizations
= 0.8.9. =
This is the initial release version of the plugin.
== Upgrade Notice ==
= 0.9.2 =
This version fixes a Woo import bug. Free shipping icon was added
= 0.9.1 =
This version provides minor optimizations in Activation section
= 0.9.0 =
This version provides minor optimizations in Activation section
= 0.8.9.1 =
This version fixes a minor Woo compatibility bug.
= 0.8.9. =
This version has updates in design, layout and functions.
== Screenshots ==
1. Search results
2. Import settings
3. Import reviews