<?php
/**
 * @author QantumThemes
 * Creates admin settings page
 */




/**
 * Create options page
 */
add_action('admin_menu', 'qt_ajax_pageload_create_optionspage');
if(!function_exists('qt_ajax_pageload_create_optionspage')){
	function qt_ajax_pageload_create_optionspage() {
		add_options_page('QT AjaxPageLoad', 'QT AjaxPageLoad', 'manage_options', 'qt_ajax_pageload_settings', 'qt_ajax_pageload_options');
	}
}

/**
 *  Main options page content
 */
if(!function_exists('qt_ajax_pageload_options')){
	function qt_ajax_pageload_options() {

		/**
		 *  We check if the use is qualified enough
		 */
		if (!current_user_can('manage_options'))  {
			wp_die( __('You do not have sufficient permissions to access this page.') );
		}

		/**
		 *  Saving options
		 */
		
		$chackboxes = array(
			// "qt_ajax_pageload_autoplay" => esc_html__("Autoplay first track [only desktop and with Ajax plugin active]", "qt-ajax-pageload" ),
		);

		$textfields = array(
			// "qt_ajax_pageload_timeout_revote" => __("Time before adding new love (minutes)", "qt-ajax-pageload" ),
		);

		$textarea = array(
			"qt_ajax_pageload_customcode" => __("Reload script", "qt-ajax-pageload" ),
		);


		if ( ! empty( $_POST ) ) {
			if(!check_admin_referer( 'qt_ajax_pageload_save', 'qt_ajax_pageload_nonce' )){
				echo 'Invalid request';
			} else {

				foreach($textfields as $varname => $label){
					if(isset($_POST[$varname])){
						update_option($varname, wp_kses($_POST[$varname], array() ));
					}
				}
				foreach($textarea as $varname => $label){
					if(isset($_POST[$varname])){
						update_option($varname, wp_kses($_POST[$varname], array() ));
					}
				}

				foreach($chackboxes as $varname => $label){
					if(isset($_POST[$varname])){
						if($_POST[$varname] == 'on'){
							update_option($varname, 1);
						} 
					} else {
						update_option($varname, 0 );
					}
				}
			}
		}

		/**
		 *  Options page content
		 */
		?>
		<div class="qt_ajax_pageload-framework qt_ajax_pageload-optionspage">
			<h1>QT AjaxPageLoad Settings</h1>
			<p class="right blue-grey-text lighten-3">V. <?php echo esc_attr(qt_ajax_pageload_get_version()); ?></p>
			<h3>Add your reload script here.<br><span style="color:red">Please be careful, this will output unescaped javascript. The support doesn't cover bad custom javascript.</span></h3>
			<div class="row">
				<form method="post" class="col s12" action="<?php echo esc_url($_SERVER["REQUEST_URI"]); ?>">
					<?php
					foreach($chackboxes as $varname => $label){
					?>
						<p class="row">
							<input id="<?php echo esc_attr($varname); ?>" name="<?php echo esc_attr($varname); ?>"  type="checkbox" <?php if (get_option( $varname)){ ?> checked <?php } ?>>
							<label for="<?php echo esc_attr($varname); ?>"><?php echo esc_attr($label); ?></label>
						</p>
					<?php } ?>
					<?php
					foreach($textfields as $varname => $label){
					?>
						<p class="row">
							<label for="<?php echo esc_attr($varname); ?>"><?php echo esc_attr($label); ?></label><br>
							<input id="<?php echo esc_attr($varname); ?>" name="<?php echo esc_attr($varname); ?>"  type="text" value="<?php echo esc_attr(get_option( $varname, 120)); ?>">
						</p>
					<?php } ?>

					<?php
					foreach($textarea as $varname => $label){
					?>
						<p class="row">
							<label for="<?php echo esc_attr($varname); ?>"><?php echo esc_attr($label); ?></label><br>
							<textarea style="width: 70%;height:500px;" id="<?php echo esc_attr($varname); ?>" name="<?php echo esc_attr($varname); ?>"><?php echo stripslashes(get_option( $varname)); ?></textarea>
						</p>
					<?php } ?>
					<?php wp_nonce_field( "qt_ajax_pageload_save", "qt_ajax_pageload_nonce", true, true ); ?>
					<input type="submit" name="submit" value="Save"  class="button button-primary" />
				</form>
			</div>
			
		</div>
		<?php 
	}
}