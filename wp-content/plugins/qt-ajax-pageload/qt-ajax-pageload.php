<?php  
/*
Plugin Name: QT Ajax Pageload
Plugin URI: http://qantumthemes.com
Description: Adds page load with ajax to keep music playing across pages
Version: 2.4
Author: QantumThemes
Author URI: http://qantumthemes.com
*/


/**
 * 	constants
 * 	=============================================
 */
if(!defined('QT_APL_PLUGIN_ACTIVE')) {
	define('QT_APL_PLUGIN_ACTIVE', true);
}
if(!defined('QT_APL_BASE_DIR')) {
	define('QT_APL_BASE_DIR', dirname(__FILE__));
}
if(!defined('QT_APL_BASE_URL')) {
	define('QT_APL_BASE_URL', plugin_dir_url(__FILE__));
}

/**
* Returns current plugin version.
* @return string Plugin version. Needs to stay here because of plugin file path
*/
if(!function_exists('qt_ajax_pageload_get_version')){
function qt_ajax_pageload_get_version() {
	if ( is_admin() ) {
		$plugin_data = get_plugin_data( __FILE__ );
		$plugin_version = $plugin_data['Version'];
	} else {
		$plugin_version = get_file_data( __FILE__ , array('Version'), 'plugin');
	}
	return $plugin_version;
}}




/**
 * @since  2.4
 * Output custom javascript
 */
if(!function_exists('qt_ajax_pageload_customscript_output')){
	function qt_ajax_pageload_customscript_output(){
		if(isset($_GET)){
			if(array_key_exists('qt-ajax-pageload-custom', $_GET)){
				if( $_GET['qt-ajax-pageload-custom'] == 'output' ){
					header('Content-Type: application/javascript');
					echo stripslashes( get_option( 'qt_ajax_pageload_customcode' ) );
					die();
				}
			}
		}
	}
	qt_ajax_pageload_customscript_output();
}

/**
 * @since  2.4
 * Output a DIV with the script URL that will be used by the ajax script to load the custom file
 */
if(!function_exists('qt_ajax_pageload_customscript_url')){
	add_action("wp_footer", "qt_ajax_pageload_customscript_url");
	function qt_ajax_pageload_customscript_url(){
		$custom_script_url = home_url( add_query_arg( 'qt-ajax-pageload-custom', 'output'  ));
		?>
		<div id="qt-ajax-customscript-url" class="qt-hidden" data-customscripturl="<?php echo $custom_script_url; ?>"></div>
		<?php  
	}
}



/**
 * 	includes
 * 	=============================================
 */
// Admin
require plugin_dir_path( __FILE__ ) . '/_qt-ajax-pageload-admin.php';

/**
 * 	Enqueue scripts
 * 	=============================================
 */
if(!function_exists('qtapl_enqueue_stuff')){
function qtapl_enqueue_stuff(){
	if(is_user_logged_in()){
		if(current_user_can('edit_pages' )){
			return;
		}
	}
	wp_enqueue_style('qt_ajax_pageload_style', QT_APL_BASE_URL.'qt-apl-style.css' );

	/**
	 * @since 2.4 2019 11 27
	 * Custom script added to the ajax loading
	 */
	// $custom_script_url = home_url( add_query_arg( 'qt-ajax-pageload-custom', 'output'  ));

	// wp_enqueue_script('qt_ajax_pageload_custom_script', $custom_script_url, array('jquery', 'qantumthemes_main'), '1.0', true );
	


	if(get_theme_mod("qt_enable_debug", 0)){
		wp_enqueue_script('qt_ajax_pageload_script', QT_APL_BASE_URL.'js/qt-ajax-pageload.js', array('jquery', 'qantumthemes_main'), '2.4', true );
	} else {
		wp_enqueue_script('qt_ajax_pageload_script', QT_APL_BASE_URL.'js/min/qt-ajax-pageload-min.js', array('jquery', 'qantumthemes_main'), '2.4', true );
	}
	
}}
add_action( 'wp_enqueue_scripts', 'qtapl_enqueue_stuff' );

/**
 * 	Skip ajax pageload custom field
 * 	=============================================
 */

if(!function_exists("qtapl_add_special_fields")){
	add_action('init', 'qtapl_add_special_fields',0,999);  
	function qtapl_add_special_fields() {
	    $qtapl_settings = array (
	    	array (
				'label' => esc_attr__('Disable ajax loading',"qt-ajax-pageload"),
				'desc' 	=> esc_attr__('Load this page without ajax (music stops, provide better plugins compatibility)',"qt-ajax-pageload"),
				'id' 	=> 'qtapl_skip',
				'type' 	=> 'checkbox'
			)        
	    );
	    if(post_type_exists('page')){
	        if(function_exists('custom_meta_box_field')){
	            $main_box = new custom_add_meta_box('qtapl_settings', 'Ajax loading settings', $qtapl_settings, 'page', true );
	        }
	    }
	}
}



/* Add user agent to body for css classes fix
=============================================*/
if ( ! function_exists( 'qtapl_class_names' ) ) {
	add_filter( 'body_class','qtapl_class_names' );
	function qtapl_class_names( $classes ) {
		if( is_singular() || is_page() ) {
			if( get_post_meta( get_the_ID(), 'qtapl_skip', true ) ){
				$classes[] = "qtapl-skip";
			}
		} 
		return $classes;
	}
}


