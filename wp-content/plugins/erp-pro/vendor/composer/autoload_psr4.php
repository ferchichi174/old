<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'infobip\\api\\model\\sms\\mt\\send\\textual\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/sms/mt/send/textual'),
    'infobip\\api\\model\\sms\\mt\\send\\binary\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/sms/mt/send/binary'),
    'infobip\\api\\model\\sms\\mt\\send\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/sms/mt/send'),
    'infobip\\api\\model\\sms\\mt\\reports\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/sms/mt/reports'),
    'infobip\\api\\model\\sms\\mt\\logs\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/sms/mt/logs'),
    'infobip\\api\\model\\sms\\mt\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/sms/mt'),
    'infobip\\api\\model\\sms\\mo\\reports\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/sms/mo/reports'),
    'infobip\\api\\model\\sms\\mo\\logs\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/sms/mo/logs'),
    'infobip\\api\\model\\sms\\mo\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/sms/mo'),
    'infobip\\api\\model\\sms\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/sms'),
    'infobip\\api\\model\\nc\\query\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/nc/query'),
    'infobip\\api\\model\\nc\\notify\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/nc/notify'),
    'infobip\\api\\model\\nc\\logs\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/nc/logs'),
    'infobip\\api\\model\\nc\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/nc'),
    'infobip\\api\\model\\account\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model/account'),
    'infobip\\api\\model\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/model'),
    'infobip\\api\\configuration\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/configuration'),
    'infobip\\api\\client\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api/client'),
    'infobip\\api\\' => array($vendorDir . '/infobip/infobip-api-php-client/infobip/api'),
    'Wikimedia\\Composer\\' => array($vendorDir . '/wikimedia/composer-merge-plugin/src'),
    'WeDevs\\ERP_PRO\\PRO\\' => array($baseDir . '/modules/pro'),
    'WeDevs\\ERP_PRO\\HRM\\' => array($baseDir . '/modules/hrm'),
    'WeDevs\\ERP_PRO\\CRM\\' => array($baseDir . '/modules/crm'),
    'WeDevs\\ERP_PRO\\ACC\\' => array($baseDir . '/modules/accounting'),
    'WeDevs\\ERP_PRO\\' => array($baseDir . '/includes'),
    'WeDevs\\ERP\\Workflow\\' => array($baseDir . '/modules/hrm/workflow/includes'),
    'WeDevs\\ERP\\Salesforce\\' => array($baseDir . '/modules/pro/salesforce/includes'),
    'WeDevs\\ERP\\SMS\\' => array($baseDir . '/modules/hrm/sms-notification/includes'),
    'WeDevs\\ERP\\Mailchimp\\' => array($baseDir . '/modules/pro/mailchimp/includes'),
    'WeDevs\\ERP\\Hubspot\\' => array($baseDir . '/modules/pro/hubspot/includes'),
    'WeDevs\\ERP\\Accounting\\Payment_Gateway\\' => array($baseDir . '/modules/accounting/payment-gateway/includes'),
    'Stripe\\' => array($vendorDir . '/stripe/stripe-php/lib'),
    'Clickatell\\' => array($vendorDir . '/arcturial/clickatell/src', $vendorDir . '/arcturial/clickatell/test'),
);
