��            )   �      �     �     �     �     �     �  +   �  A     &   _     �  
   �     �  
   �  i  �          "     /  N   =     �     �  Z   �  ?     <   G  P   �  ,   �  �        �            5   &  (  \     �	     �	     �	     �	     �	  1   �	  D   
  #   P
     t
  
   �
     �
  
   �
  m  �
  	        #     7  p   I     �     �  a   �  8   E  B   ~  d   �  7   &  �   ^     C     X     d  5   x           
                                                          	                                                                    "%1$s" by %2$s. "%1$s" from %2$s by %3$s. "%1$s" from %2$s. "%s". %1$s by %2$s. <em>%s</em> has been added to Media library <em>%s</em> was <strong>not</strong> imported due to an error: %s <strong>Current Directory:</strong> %s Add From Server Dion Hulse File Genre: %s. Hi there!
I notice you use WordPress in a Language other than English (US), Did you know you can translate WordPress Plugins into your native language as well?
If you'd like to help out with translating this plugin into %1$s you can head over to <a href="%2$s">translate.WordPress.org</a> and suggest translations for any languages which you know.
Thanks! Dion. Import Import Files Parent Folder Plugin to allow the Media Manager to add files from the webservers filesystem. Released: %d. Show hidden files Sorry, but this file is unreadable by your Webserver. Perhaps check your File Permissions? Sorry, that file already exists in the WordPress media library. Sorry, this file type is not permitted for security reasons. Sorry, this file type is not permitted for security reasons. Please see the FAQ. The selected file could not be copied to %s. This plugin requires WordPress %1$s or greater, and PHP %2$s or greater. You are currently running WordPress %3$s and PHP %4$s. Please contact your website host or server administrator for more information. The plugin has been deactivated. Track %1$s of %2$s. Track %1$s. https://dd32.id.au/ https://dd32.id.au/wordpress-plugins/add-from-server/ PO-Revision-Date: 2020-09-24 12:00:38+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - Add From Server - Stable (latest release)
 "%1$s" door %2$s. "%1$s" van %2$s door %3$s. "%1$s" van %2$s. "%s". %1$s door %2$s. <em>%s</em> is toegevoegd aan de Mediabibliotheek <em>%s</em> is <strong>niet</strong> geïmporteerd door een fout: %s <strong>Huidige folder:</strong> %s Voeg toe vanaf de server Dion Hulse Bestand Genre: %s. Hallo,
Ik zie dat je WordPress gebruikt in een andere taal dan Engels (US). Wist je dat je WordPress Plugins ook in je moederstaal kunt vertalen?
Als je ons wilt helpen met de vertaling in het %1$s, kun je op <a href="%2$s">translate.WordPress.org pagina</a> suggesties voor vertalingen indienen voor elke taal waarmee je bekend bent.
Hartelijk dank daarvoor. Dion. Importeer Importeer bestanden Bovenliggende map Plugin om toe te staan dat de Media Manager bestanden kan toevoegen vanuit het bestandssysteem van de webserver. Uitgekomen: %d. Toon verborgen bestanden Sorry, dit bestand kan niet worden gelezen door je webserver. Controleer de lees-/schrijfrechten. Dat bestand bestaat al in de WordPress mediabibliotheek. Sorry, dit bestandstype is om beveiligingsredenen niet toegestaan. Sorry, dit bestand is niet toegestaan vanwege beveiligingsredenen. Lees de FAQ voor meer informatie. Het gekozen bestand kan niet gekopieerd worden naar %s. Deze plugin vereist Wordpress %1$s of hoger, en PHP %2$s of hoger. Je gebruikt momenteel WordPress %3$s en PHP %4$s. Neem voor meer informatie contact op met je webhosting bedrijf of websitebeheerder. De plugin is gedeactiveerd. Track %1$s van %2$s. Track %1$s. https://dd32.id.au/ https://dd32.id.au/wordpress-plugins/add-from-server/ 