<?php
/*
Package: OnAir2
Description: SLIDESHOW POST
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/
?>
<!-- SLIDESHOW POST ================================================== -->
<div class="qt-slickslider-container qt-slickslider-externalarrows">
	<div class="row">
		<div class="qt-slickslider qt-slickslider-multiple qt-text-shadow " data-slidestoshow="3" data-variablewidth="false" data-arrows="true" data-dots="false" data-infinite="true" data-centermode="false" data-pauseonhover="true" data-autoplay="false" data-arrowsmobile="false"  data-centermodemobile="true" data-dotsmobile="false"  data-slidestoshowmobile="1" data-variablewidthmobile="true" data-infinitemobile="false">
			<!-- SLIDESHOW ITEM -->
			<div class="qt-item">
				<?php get_template_part (  'phpincludes/part-archive-item-post-vertical.php'); ?>
			</div>
			<!-- SLIDESHOW ITEM -->
			<div class="qt-item">
				<?php get_template_part (  'phpincludes/part-archive-item-post-vertical.php'); ?>
			</div>
			<!-- SLIDESHOW ITEM -->
			<div class="qt-item">
				<?php get_template_part (  'phpincludes/part-archive-item-post-vertical.php'); ?>
			</div>
			<!-- SLIDESHOW ITEM -->
			<div class="qt-item">
				<?php get_template_part (  'phpincludes/part-archive-item-post-vertical.php'); ?>
			</div>
			<!-- SLIDESHOW ITEM -->
			<div class="qt-item">
				<?php get_template_part (  'phpincludes/part-archive-item-post-vertical.php'); ?>
			</div>
			<!-- SLIDESHOW ITEM -->
			<div class="qt-item">
				<?php get_template_part (  'phpincludes/part-archive-item-post-vertical.php'); ?>
			</div>
		</div>
	</div>
</div>
<!-- SLIDESHOW POST END ================================================== -->
